package pt.ulisboa.tecnico.cmov.notepadapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CreateNoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
    }

    // method call when the ok button is clicked
    public void addNote(View view){
        TextView noteTitleView = (TextView) findViewById(R.id.editText_noteTitle);
        TextView noteContentView = (TextView) findViewById(R.id.editText_noteContent);

        String noteTitle = noteTitleView.getText().toString();
        String noteContent = noteContentView.getText().toString();

        if (noteTitle.equals("") || noteContent.equals("")) {
            Toast.makeText(getBaseContext(), "Missing fields!", Toast.LENGTH_LONG).show();
        }
        else{
            Intent data = new Intent();
            data.putExtra("noteTitle", noteTitle);
            data.putExtra("noteContent", noteContent);
            setResult(RESULT_OK, data);
            finish();
        }
    }

    // method call when the cancel button is clicked
    public void cancel(View view){
        finish();
    }
}
