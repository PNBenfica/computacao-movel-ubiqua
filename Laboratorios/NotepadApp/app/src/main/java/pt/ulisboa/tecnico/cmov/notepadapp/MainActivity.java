package pt.ulisboa.tecnico.cmov.notepadapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> notesList;
    private ArrayList<String> notesContent = new ArrayList<String>();
    private ListView notesListView;
    private ArrayAdapter arrayAdapter;

    public final static String NOTE_TITLE = "pt.ulisboa.tecnico.cmov.helloworld.NOTE_ID";
    public final static String NOTE_CONTENT = "pt.ulisboa.tecnico.cmov.helloworld.NOTE_CONTENT";

    static final int NEW_NOTE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeNotesList();
    }

    // start the activity to add a new note
    public void addNewNote(View v){
        Intent intent = new Intent(this, CreateNoteActivity.class);
        startActivityForResult(intent, NEW_NOTE_REQUEST);
    }

    public void addNote(String noteTile, String noteContent){
        int position = notesList.size();
        notesList.add(noteTile);
        notesContent.add(noteContent);
        this.arrayAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == NEW_NOTE_REQUEST) {
            if (data.hasExtra("noteTitle") && data.hasExtra("noteContent")) {
                String noteTile = data.getExtras().getString("noteTitle");
                String noteContent = data.getExtras().getString("noteContent");
                addNote(noteTile, noteContent);
            }
        }
    }

    private void initializeNotesList() {
        this.notesList = new ArrayList<String>();
        this.notesListView = (ListView) findViewById(R.id.notesListView);

        this.arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                this.notesList );

        this.notesListView.setAdapter(arrayAdapter);

        addNote("Nota 1", "Conteudo da nota 1");
        addNote("Nota 2", "Conteudo da nota 2");

        this.notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(getApplication(), ReadNoteActivity.class);
                intent.putExtra(NOTE_TITLE, item);
                intent.putExtra(NOTE_CONTENT, notesContent.get(position));

                startActivity(intent);
            }
        });
    }
}
