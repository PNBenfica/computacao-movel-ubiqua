package pt.ulisboa.tecnico.cmov.notepadapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ReadNoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_note);

        // Get the message from the intent
        Intent intent = getIntent();
        String note_title = intent.getStringExtra(MainActivity.NOTE_TITLE);
        String note_content = intent.getStringExtra(MainActivity.NOTE_CONTENT);

        TextView noteTitleView = (TextView) findViewById(R.id.textView_note_title);
        TextView noteContentView = (TextView) findViewById(R.id.textView_note_content);

        noteTitleView.setText(note_title);
        noteContentView.setText(note_content);
    }
}
