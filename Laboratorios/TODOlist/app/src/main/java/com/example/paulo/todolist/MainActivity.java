package com.example.paulo.todolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> tasksList;
    private ListView tasksListView;
    private EditText newTaskText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initializeTaskList();

        newTaskText = (EditText) findViewById(R.id.editText);
        newTaskText.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    addNewTask();
                }
                return keyCode == KeyEvent.KEYCODE_ENTER;
            }
        });

        Button addTaskButton = (Button) findViewById(R.id.add_task_button);
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                addNewTask();
            }
        });
    }

    private void initializeTaskList() {
        this.tasksList = new ArrayList<String>();
        this.tasksListView = (ListView) findViewById(R.id.todo_list_view);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                this.tasksList );

        this.tasksListView.setAdapter(arrayAdapter);

        this.tasksListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, int position,
                                        long id) {


                final String item = (String) parent.getItemAtPosition(position);
                view.animate().setDuration(500).alpha(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                tasksList.remove(item);
                                arrayAdapter.notifyDataSetChanged();
                                view.setAlpha(1);
                            }
                        });

                //tasksList.remove(position);
                //ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) tasksListView.getAdapter();
                //arrayAdapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    private void addNewTask() {
        String newTask = this.newTaskText.getText().toString();
        if (!newTask.equals("")){
            this.tasksList.add(0, newTask);
            this.newTaskText.setText("");
        }
    }
}
