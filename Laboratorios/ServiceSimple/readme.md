Lab 2 
Exercise II: Services

The goal of this exercise is to understand how services work.
Use the application context to hold a counter for the number of times the service has been started.