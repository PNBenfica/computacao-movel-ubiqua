package pt.ulisboa.tecnico.cmov.servicesimple;

import android.app.Application;

public class Counter extends Application{

    private int counter = 0;

    public void incCounter(){
        this.counter++;
    }

    public int getCounter(){
        return counter;
    }
}