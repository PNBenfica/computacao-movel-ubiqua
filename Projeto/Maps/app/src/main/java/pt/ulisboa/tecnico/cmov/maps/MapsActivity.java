package pt.ulisboa.tecnico.cmov.maps;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Button btnShowLocation;
    private boolean running = false;
    GPSTracker gps;
    PolylineOptions polylineOptions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        gps = new GPSTracker(MapsActivity.this);
        polylineOptions = new PolylineOptions().geodesic(true).width(10).color(this.getResources().getColor(R.color.cyan));
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            LatLng mylocation = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 16.0f));
            polylineOptions.add(mylocation);
            mMap.addPolyline(polylineOptions);
        }

        setUpMap();
    }


    private void setUpMap(){
        Runnable tLogic = new Runnable(){
            public void run(){

                while(true){
                    try {
                        Thread.sleep(1000);
                        clickLocation(null);

                    } catch(Exception e){}
                }
            }
        };
        Thread t =  new Thread(tLogic , "My Thread");
        t.start();
    }

    public void onclick(View view){
        running = !running;
    }

    public void clickLocation(View view){

        gps = new GPSTracker(MapsActivity.this);
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            LatLng mylocation = new LatLng(latitude,longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mylocation));
            polylineOptions.add(mylocation);
            mMap.addPolyline(polylineOptions);
        } else {
            gps.showSettingsAlert();
        }
    }





}
