package pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions;

public class ExpiredTokenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExpiredTokenException(String msg){
		super(msg);
	}
}
