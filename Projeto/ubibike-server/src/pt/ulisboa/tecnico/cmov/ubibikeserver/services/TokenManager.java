package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.security.SecureRandom;
import java.sql.SQLException;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.TokenDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

public class TokenManager {
	
	public final static int TOKEN_VALIDITY_TIME = 1; //hour

	public static String issueToken(String username) {
		String token = generateToken();
		try{
			storeToken(username, token);
		}
		catch (Exception e){
			token = null;
		}
		return token;
	}

	private static String generateToken() {
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom random = new SecureRandom();

		StringBuilder sb = new StringBuilder(50);
		for (int i = 0; i < 50; i++)
			sb.append(AB.charAt(random.nextInt(AB.length())));
		return sb.toString();
	}

	public static Token getToken(String sessionToken) throws SQLException, TokenNotFoundException, ExpiredTokenException {
		Token token = new TokenDAO().getToken(sessionToken);
		
		if (token == null)
			throw new TokenNotFoundException("Authentication token sent does not exist");
		else if (token.hasExpired())
			throw new ExpiredTokenException("Token expired");
		
		return token;
	}

	private static boolean storeToken(String username, String token) throws SQLException, Exception {
		boolean status = new TokenDAO().insert(username, token);
		return status;
	}

}
