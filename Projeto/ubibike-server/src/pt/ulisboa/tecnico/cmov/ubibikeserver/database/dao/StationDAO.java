package pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.ConnectionFactory;
import pt.ulisboa.tecnico.cmov.ubibikeserver.database.DbUtil;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Station;

public class StationDAO {
	
	private Connection dbConnection;
	private Statement statement;
	

	public Station[] getAll() throws SQLException{
		List<Station> stations = new ArrayList<Station>();
		ResultSet rs = null;
		try {
			dbConnection = ConnectionFactory.createConnection();			
			statement = dbConnection.createStatement();
			
			String query = "SELECT S.* , COUNT(*) As availableBikes"
					+ " FROM Stations S JOIN Bikes B"
					+ " WHERE S.station_id = B.station_id AND B.booked_by_user IS NULL"
					+ " GROUP BY S.station_id";
			rs = statement.executeQuery(query);
			
			while (rs.next()) {
				String id = rs.getString("station_id");
				String name = rs.getString("name");
				String location = rs.getString("location");
				int availableBikes = rs.getInt("availableBikes");
				double latitude = rs.getDouble("latitude");
				double longitude = rs.getDouble("longitude");
				
				stations.add(new Station(id, name, location, availableBikes, latitude, longitude));
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(statement);
            DbUtil.close(dbConnection);
		}
		return stations.toArray(new Station[stations.size()]);
	}
	
	public Station get(String stationId) throws SQLException{
		Station station = null;
		ResultSet rs = null;
		try {
			dbConnection = ConnectionFactory.createConnection();			
			statement = dbConnection.createStatement();
			
			String query = "SELECT S.* , COUNT(*) As availableBikes"
					+ " FROM Stations S JOIN Bikes B"
					+ " WHERE S.stationId ='" + stationId + "' AND S.stationId = B.station_id AND B.booked_by_user IS NULL";
			rs = statement.executeQuery(query);
			
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				String location = rs.getString("location");
				int availableBikes = rs.getInt("availableBikes");
				double latitude = rs.getDouble("latitude");
				double longitude = rs.getDouble("longitude");
				
				station = new Station(id, name, location, availableBikes, latitude, longitude);
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(statement);
            DbUtil.close(dbConnection);
		}
		return station;
	}
	

	public String bookBike(String username, String stationId) throws SQLException {
		String bookedBikeId = "";
		String query;
		Connection dbConn = null;
		PreparedStatement getBikeToBookStmt = null;
		PreparedStatement bookBikeStmt = null;
		try {
			dbConn = ConnectionFactory.createConnection();
			dbConn.setAutoCommit(false);
			
			query = "SELECT Bikes.bike_id "
				 + " FROM Bikes"
				 + " WHERE Bikes.station_id = '" + stationId + "' AND Bikes.booked_by_user IS NULL AND NOT EXISTS (SELECT Bikes.booked_by_user FROM Bikes WHERE Bikes.booked_by_user= '" + username + "')";

			getBikeToBookStmt = dbConn.prepareStatement(query); // get bike to book and if user is allowed to (if doesn't have other bike booked)

			ResultSet rs = getBikeToBookStmt.executeQuery();	
			
			if(rs.next()) {
				String bikeToBookId = rs.getString("bike_id");
				
				query = "UPDATE Bikes "
					 + " SET Bikes.booked_by_user = '" + username + "'"
					 + " WHERE Bikes.bike_id = '" + bikeToBookId + "' AND Bikes.station_id = '" + stationId + "'";
				bookBikeStmt = dbConn.prepareStatement(query);
				int records = bookBikeStmt.executeUpdate();
				if (records > 0) {
					bookedBikeId = bikeToBookId;
				}
			}

			dbConn.commit();
		} finally {
            DbUtil.close(getBikeToBookStmt);
            DbUtil.close(bookBikeStmt);
            DbUtil.close(dbConn);
		}
		return bookedBikeId;
	}
	

	public boolean dropBike(String username, String stationId) throws SQLException {
		boolean successfulInsert = false;
		Connection dbConn = null;
		Statement stmt = null;
		try {
			dbConn = ConnectionFactory.createConnection();			
			stmt = dbConn.createStatement();
			
			String query = "UPDATE Bikes"
						 + " SET Bikes.booked_by_user = NULL, station_id = '"+stationId+"'"
						 + " WHERE Bikes.booked_by_user='" + username +"'";
			int records = stmt.executeUpdate(query);
			
			//When record is successfully inserted
			if (records > 0) {
				successfulInsert = true;
			}
		} finally {
            DbUtil.close(stmt);
            DbUtil.close(dbConn);
		}
		return successfulInsert;
	}

}
