package pt.ulisboa.tecnico.cmov.ubibikeserver.domain;

import org.codehaus.jettison.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibikeserver.services.Utility;

public class Coordinate {
	
	private double latitude;
	
	private double longitude;
	
	public Coordinate(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public void setLatitude(double latitude){
		this.latitude = latitude;
	}
	
	public void setLongitude(double longitude){
		this.longitude = longitude;
	}
	
	public double getLatitude(){
		return this.latitude;
	}
	
	public double getLongitude(){
		return this.longitude;
	}

	public JSONObject convertToJSON(){
		return Utility.constructJSONObj("latitude", this.getLatitude(), "longitude", this.getLongitude());
	}
}
