package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.net.URLDecoder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.UserDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.User;

@Path("/login")
public class LoginService {
	
	@SuppressWarnings("deprecation")
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8080/ubibike-server/login/?username=paulo&password=1234&publicKey=pubkey
	public String doLogin(@QueryParam("username") String uname, @QueryParam("password") String pwd, @QueryParam("publicKey") String publicKey){
		System.out.println("|Login| Username: " + uname);
		
		String response = "";
		
		if (publicKey != null){
			publicKey = URLDecoder.decode(publicKey).replaceAll(" ", "+").replaceAll("\n", "");
		}
		
		User user = new UserDAO().login(uname, pwd, publicKey);		
		
		if(user != null){ // credentials were right
			String token = TokenManager.issueToken(uname);
			
			if (token == null){
				response = Utility.constructJSON("LoginService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
			}
			else{
				response = Utility.constructJSON("LoginService",true, "token", token, "points", user.getPoints());
			}
		}else{
			response = Utility.constructJSON("LoginService", false, "error_code", ErrorCodes.INVALID_CREDENTIALS.getCode(), "error_msg", ErrorCodes.INVALID_CREDENTIALS.getMessage());
		}
	return response;		
	}
	
}
