package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.TrajectoryDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Coordinate;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Trajectory;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

@Path("/trajectories")
public class TrajectoryService {
	
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8080/ubibike-server/trajectories
	public String getTrajectories(@HeaderParam("Authorization") String sessionToken) {
		
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			System.out.println("|Get Trajectories| User: " + token.getUsername());
			JSONArray trajectories = getAllTrajectories(token);
			response = Utility.constructJSON("TrajectoryService", true, "trajectories", trajectories);
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("TrajectoryService", false, "error_code", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("TrajectoryService", false, "error_code", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("TrajectoryService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON) 
	@Path("/newtrajectory")
	// http://localhost:8080/ubibike-server/trajectories/newtrajectory?date=15/05/2016&distance=1256&points=100&coord=120.3;123.5&coord=120.6;124.5
	public String newTrajectory(@QueryParam("date") String date, @QueryParam("distance") double distance, @QueryParam("points") int points, @QueryParam("coord") List<String> coordinates, @HeaderParam("Authorization") String sessionToken) {
		System.out.println("|New trajectory|" + date + " " + distance + "km " + points + " points" + " Num Coordinates:" + coordinates.size());
		
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			boolean sucessfullInsert = storeNewTrajectory(token, date, distance, points, coordinates);
			if (sucessfullInsert)
				response = Utility.constructJSON("TrajectoryService", true);
			else
				response = Utility.constructJSON("TrajectoryService", false, "error_msg", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("TrajectoryService", false, "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("TrajectoryService", false, "error_msg", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("TrajectoryService", false, "error_msg", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}	

	
	private JSONArray getAllTrajectories(Token token) throws SQLException {
		String username = token.getUsername();
		Trajectory[] trajectories = new TrajectoryDAO().getAll(username);
		
		JSONObject[] JSONTrajectories = new JSONObject[trajectories.length];
		for(int i = 0; i < trajectories.length; i++){
			JSONTrajectories[i] = trajectories[i].convertToJSON();
		}
		return Utility.constructJSONArray(JSONTrajectories);
	}

	
	private boolean storeNewTrajectory(Token token, String date, double distance, int points, List<String> coords) {
		String username = token.getUsername();
		List<Coordinate> coordinates = new ArrayList<Coordinate>();
		for(String coord: coords){
			String[] coordSplit = coord.split(";");
			double latitude = Double.parseDouble(coordSplit[0]);
			double longitude = Double.parseDouble(coordSplit[1]);
			Coordinate coordinate = new Coordinate(latitude, longitude);
			coordinates.add(coordinate);
		}
		boolean sucessfullInsert = new TrajectoryDAO().insert(username, date, distance, points, coordinates);
		return sucessfullInsert;
	}
}
