package pt.ulisboa.tecnico.cmov.ubibikeserver.domain;

import org.codehaus.jettison.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibikeserver.services.Utility;

public class Station {
	
	private String id;
	
	private String name;
	
	private String location;
	
	private Integer availableBikes;
	
	private Double latitude;
	
	private Double longitude;
	
	public Station(String id, String name, String location, Integer availableBikes, Double latitude, Double longitude){
		this.id = id;		
		this.name = name;		
		this.location = location;		
		this.availableBikes = availableBikes;	
		this.latitude = latitude;	
		this.longitude = longitude;		
	}

	public String getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getLocation(){
		return this.location;
	}
	
	public Integer getAvailableBikes(){
		return this.availableBikes;
	}
	
	public Double getLatitude(){
		return this.latitude;
	}
	
	public Double getLongitude(){
		return this.longitude;
	}

	public JSONObject convertToJSON(){
		return Utility.constructJSONObj("id", this.getId(), "name", this.getName(), "location", this.getLocation(), "availableBikes", this.getAvailableBikes(), "latitude", this.getLatitude(), "longitude", this.getLongitude());
	}
	
}
