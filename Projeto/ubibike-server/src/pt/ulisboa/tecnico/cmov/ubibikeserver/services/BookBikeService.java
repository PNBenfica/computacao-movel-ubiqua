package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.StationDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.BookingBikeException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

@Path("/bookbike")
public class BookBikeService {

	@GET
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8080/ubibike-server/bookbike?stationid=station-3
	public String bookBike(@QueryParam("stationid") String stationId, @HeaderParam("Authorization") String sessionToken) {
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			System.out.println("|Book Bike| Station Id: " + stationId + " User: " + token.getUsername());
			String bookedBikeId = BookBike(token.getUsername(), stationId);
			response = Utility.constructJSON("BookBikeService", true, "bike-id", bookedBikeId);
			
		}
		catch (BookingBikeException e){
			response = Utility.constructJSON("BookBikeService", false, "error_code", ErrorCodes.BOOK_BIKE_FAILED.getCode(), "error_msg", ErrorCodes.BOOK_BIKE_FAILED.getMessage());
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("BookBikeService", false, "error_code", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("BookBikeService", false, "error_code", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("BookBikeService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}	

	private String BookBike(String userName, String stationId) throws SQLException, BookingBikeException {
		String bookedBike = new StationDAO().bookBike(userName, stationId);
		if (bookedBike.equals(""))
			throw new BookingBikeException("Station doesn't exist or has no bikes to book");
		else
			return bookedBike;
	}
}
