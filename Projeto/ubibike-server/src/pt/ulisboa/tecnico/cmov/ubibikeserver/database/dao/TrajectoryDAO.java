package pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.ConnectionFactory;
import pt.ulisboa.tecnico.cmov.ubibikeserver.database.DbUtil;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Coordinate;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Trajectory;

public class TrajectoryDAO {
	
	private Connection dbConnection;
	private Statement statement;
	

	public Trajectory[] getAll(String username) throws SQLException{
		TreeMap<Integer, Trajectory> trajectoriesMap = new TreeMap<Integer, Trajectory>();
		ResultSet rs = null;
		try {
			dbConnection = ConnectionFactory.createConnection();			
			statement = dbConnection.createStatement();
			
			String query = "SELECT T.*, C.latitude, C.longitude FROM Users U JOIN Trajectories T JOIN Coordinates C WHERE U.username = T.username AND U.username = '"+username+"' AND T.trajectory_id = C.trajectory_id";
			
			rs = statement.executeQuery(query);
			
			while (rs.next()) {
				Integer id = rs.getInt("trajectory_id");
				if (!trajectoriesMap.containsKey(id)){
					String date = rs.getString("date");
					double distance = rs.getDouble("distance");
					int points = rs.getInt("trajetory_points");						
					trajectoriesMap.put(id, new Trajectory(date, points, distance));
				}
				double latitude = rs.getDouble("latitude");
				double longitude = rs.getDouble("longitude");
				Coordinate coordinate = new Coordinate(latitude, longitude);
				trajectoriesMap.get(id).addCoordinate(coordinate);
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(statement);
            DbUtil.close(dbConnection);
		}
		ArrayList<Trajectory> trajectories = new ArrayList<Trajectory>();
		for(Trajectory trajectory: trajectoriesMap.values())
			trajectories.add(trajectory);
		return trajectories.toArray(new Trajectory[trajectories.size()]);
	}


	public boolean insert(String username, String date, double distance, int points, List<Coordinate> coordinates) {
		boolean successfulInsert = false;
		Connection dbConn = null;
		ResultSet generatedKeys = null;
		PreparedStatement statement = null;
		PreparedStatement updateUserPointsStmt = null;
		String query;
		try {
			dbConn = ConnectionFactory.createConnection();	
			
			dbConn.setAutoCommit(false);
			
			query = "UPDATE Users SET points = points + "+ points +" WHERE username = '" + username + "'";
			updateUserPointsStmt = dbConn.prepareStatement(query);
			updateUserPointsStmt.executeUpdate();
			
			query = "INSERT into Trajectories(username, date, distance, trajetory_points) values('"+username+"','"+date+"', '"+distance+"', '"+points+"')";
			statement = dbConn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			int affectedRows = statement.executeUpdate();
			generatedKeys = statement.getGeneratedKeys();
			
			if (affectedRows > 0 && generatedKeys.next()){
				
				long trajectoryId = generatedKeys.getLong(1);
			
				query = "INSERT into Coordinates(trajectory_id, latitude, longitude) values(?,?,?)";
				statement = dbConn.prepareStatement(query);	
				
				
				for(Coordinate coordinate: coordinates){
					
					statement.setLong(1, trajectoryId);
					statement.setDouble(2, coordinate.getLatitude());
					statement.setDouble(3, coordinate.getLongitude());
					
					statement.executeUpdate();					
				}
				successfulInsert = true;
			}
			dbConn.commit();
		}catch (SQLException e ) {
			successfulInsert = false;
	        if (dbConn != null) {
	            try {
	                System.err.println("Transaction is being rolled back");
	                dbConn.rollback();
	            } catch(SQLException excep) {
	            }
	        }
	    } finally {
            DbUtil.close(updateUserPointsStmt);
            DbUtil.close(dbConn);
            DbUtil.close(dbConn);
            DbUtil.close(generatedKeys);
		}
		return successfulInsert;
	}

}
