package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Utility {
	/**
	 * Null check Method
	 * 
	 * @param txt
	 * @return
	 */
	public static boolean isNotNull(String txt) {
		// System.out.println("Inside isNotNull");
		return txt != null && txt.trim().length() >= 0 ? true : false;
	}
	

	public static String constructJSON(String tag, boolean status, Object... args) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("tag", tag);
			obj.put("status", new Boolean(status));
			for (int i = 0; i < args.length; i += 2){
				obj.put((String) args[i], args[i + 1]);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		return obj.toString();
	}
	

	public static JSONObject constructJSONObj(Object... args) {
		JSONObject obj = new JSONObject();
		try {
			for (int i = 0; i < args.length; i += 2){
				obj.put((String) args[i], args[i + 1]);
			}
		} catch (JSONException e) {
		}
		return obj;
	}

	public static JSONArray constructJSONArray(JSONObject[] args) {
		JSONArray array = new JSONArray();
		for (Object obj: args){
			array.put(obj);
		}
		return array;
	}
	
}
