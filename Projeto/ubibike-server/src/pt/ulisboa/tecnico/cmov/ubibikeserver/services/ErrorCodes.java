package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

public enum ErrorCodes {

	OPERATION_FAILED(1501, "Something went wrong. A connection or database error has occured."),
	
	TOKEN_NOT_FOUND(1502, "Token provided is not associated with any client"),
	TOKEN_EXPIRED(1503, "Token has expired"),
	
	USER_ALREADY_REGISTRED(1504, "User is already registred in the platform"),
	SPECIAL_CHARS_FOUND(1505, "String provided has illegal chars"),
	INVALID_CREDENTIALS(1506, "Credentials provided are invalid"),
	
	STATION_NOT_FOUND(1507, "Station doesn't exist"),
	
	BOOK_BIKE_FAILED(1508, "Station doesn't exist or has no bikes to book"),
	
	SEND_POINTS_ERROR(1509, "User doesn't have enough points to send to the other user, or other user has already updated the server");

	private final Integer code;
	private final String message;

	ErrorCodes(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
