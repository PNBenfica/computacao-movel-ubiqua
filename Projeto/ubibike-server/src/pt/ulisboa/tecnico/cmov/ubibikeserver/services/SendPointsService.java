package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.UserDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

@Path("/")
public class SendPointsService {
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	@Path("/sendpoints")
	// http://localhost:8080/ubibike-server/sendpoints?username=paulo&points=120&transactionid=randomstring&signature=signature
	// transaction id is used to guarantee that this update only occurs one time (first user to contact server)
	public String sendPoints(@QueryParam("username") String username, @QueryParam("points") int points, @QueryParam("transactionid") String transactionid, @QueryParam("signature") String signature, @HeaderParam("Authorization") String sessionToken) {
		
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			System.out.println("|Send Points| From: " + token.getUsername() + " To: " + username);
			response = sendPoints(token.getUsername(), username, points, transactionid, signature);			
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	@Path("/receivepoints")
	// http://localhost:8888/ubibike-server/receivepoints?username=paulo&points=120&transactionid=randomstring
	// transaction id is used to guarantee that this update only occurs one time (first user to contact server)
	public String receivePoints(@QueryParam("username") String username, @QueryParam("points") int points, @QueryParam("transactionid") String transactionid, @QueryParam("signature") String signature, @HeaderParam("Authorization") String sessionToken) {
		
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			System.out.println("|Receive Points| From: " + username + " To: " + token.getUsername());
			response = sendPoints(username, token.getUsername(), points, transactionid, signature);			
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}
	
	
	public String sendPoints(String usernameFrom, String usernameTo, int points, String transactionid, String signature){
		String response;
		try{
			int status = new UserDAO().sendPoints(usernameFrom, usernameTo, points, transactionid, signature);
			switch (status) {
			case 0:
				response = Utility.constructJSON("SendPointsService", true);
				break;
			case 1:
				response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.SEND_POINTS_ERROR.getCode(), "error_msg", ErrorCodes.SEND_POINTS_ERROR.getMessage());
				break;
			default:
				response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
				break;
			}			
		}
		catch(Exception e){
			response = Utility.constructJSON("SendPointsService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}

}
