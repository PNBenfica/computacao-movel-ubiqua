package pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions;

public class TokenNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenNotFoundException(String msg){
		super(msg);
	}
}
