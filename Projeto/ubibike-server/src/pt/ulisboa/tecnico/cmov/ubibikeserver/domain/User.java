package pt.ulisboa.tecnico.cmov.ubibikeserver.domain;

public class User {

	private String username;
	
	private Integer points;

	private String pubKey;

	public User(String username, Integer points, String pubKey){
		this.username = username;
		this.points = points;
		this.pubKey = pubKey;
	}
	
	public String getUsername() {
		return username;
	}

	public Integer getPoints() {
		return points;
	}

	public String getPublicKey() {
		return pubKey;
	}
}
