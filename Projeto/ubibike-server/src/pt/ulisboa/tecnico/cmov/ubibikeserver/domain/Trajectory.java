package pt.ulisboa.tecnico.cmov.ubibikeserver.domain;

import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibikeserver.services.Utility;

public class Trajectory {
	
	private Double distance;
	
	private Integer points;
	
	private String date;
	
	private ArrayList<Coordinate> coordinates;
	
	public Trajectory(String date, int points, double distance){
		this.date = date;
		this.points = points;
		this.distance = distance;
		this.coordinates = new ArrayList<Coordinate>();
	}
	
	public String getDate(){
		return this.date;
	}
	
	public Integer getpoints(){
		return this.points;
	}
	
	public Double getDistance(){
		return this.distance;
	}
	
	public void addCoordinate(Coordinate c){
		this.coordinates.add(c);
	}
	

	
	private JSONArray getCoordinates() {
		
		JSONObject[] JSONCoordinates = new JSONObject[coordinates.size()];
		for(int i = 0; i < coordinates.size(); i++){
			JSONCoordinates[i] = coordinates.get(i).convertToJSON();
		}
		return Utility.constructJSONArray(JSONCoordinates);
	}

	public JSONObject convertToJSON(){
		return Utility.constructJSONObj("date", this.getDate(), "distance", this.getDistance(), "points", this.getpoints(), "coordinates", this.getCoordinates());
	}

}
