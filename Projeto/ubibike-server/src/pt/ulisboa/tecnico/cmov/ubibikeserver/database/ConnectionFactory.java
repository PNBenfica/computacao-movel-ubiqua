package pt.ulisboa.tecnico.cmov.ubibikeserver.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionFactory {
	/**
	 * Method to create DB Connection
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("finally")
	public static Connection createConnection() {
		Connection con = null;
		try {
			Class.forName(DbSettings.dbClass);
			con = DriverManager.getConnection(DbSettings.dbUrl, DbSettings.dbUser, DbSettings.dbPwd);
		} catch (Exception e) {
			e.printStackTrace();;
		} finally {
			return con;
		}
	}
		
	/**
	 * Method to insert uname and pwd in DB
	 */
	public static boolean insertUser(String email, String uname, String pwd) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			dbConn = ConnectionFactory.createConnection();
			stmt = dbConn.createStatement();
			
			String query = "INSERT into Users(mail, username, password) values('"+email+ "',"+"'"
					+ uname + "','" + pwd + "')";
			
			int records = stmt.executeUpdate(query);
			
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(dbConn);
		}
		return insertStatus;
	}	
}
