package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.StationDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Station;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.StationNotFoundException;

@Path("/stations")
public class StationService {	
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8888/ubibike-server/stations?stationid=station-3
	public String getStations(@QueryParam("stationid") String stationId) {
		System.out.println("|Get Stations|");
		
		String response;
		try{
			if (stationId != null){
				JSONObject station = getStationById(stationId);
				response = Utility.constructJSON("StationService", true, "station", station);
			}
			else{
				JSONArray stations = getAllStations();
				response = Utility.constructJSON("StationService", true, "stations", stations);
			}
		}
		catch(StationNotFoundException e){
			response = Utility.constructJSON("StationService", false, "error_code", ErrorCodes.STATION_NOT_FOUND.getCode(), "error_msg", ErrorCodes.STATION_NOT_FOUND.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("StationService", false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}
	
	private JSONArray getAllStations() throws SQLException {

		Station[] stations = new StationDAO().getAll();
		
		JSONObject[] JSONStations = new JSONObject[stations.length];
		for(int i = 0; i < stations.length; i++){
			JSONStations[i] = stations[i].convertToJSON();
		}
		return Utility.constructJSONArray(JSONStations);
	}
		
	private JSONObject getStationById(String stationId) throws SQLException, StationNotFoundException {
		Station station = new StationDAO().get(stationId);	
		if (station == null)
			throw new StationNotFoundException("Station was not found");	
		return station.convertToJSON();
	}
}
