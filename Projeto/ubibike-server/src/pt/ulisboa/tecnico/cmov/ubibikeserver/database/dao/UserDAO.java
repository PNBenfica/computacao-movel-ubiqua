package pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pt.ulisboa.tecnico.cmov.ubibikeserver.crypto.RSATools;
import pt.ulisboa.tecnico.cmov.ubibikeserver.database.ConnectionFactory;
import pt.ulisboa.tecnico.cmov.ubibikeserver.database.DbUtil;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.User;
import pt.ulisboa.tecnico.cmov.ubibikeserver.services.Utility;

public class UserDAO {
	
	public User login(String username, String password, String pubKey){
		User user = null;

		Connection dbConn = null;
		PreparedStatement updatePubKeyStmt= null;
		PreparedStatement getUserStmt = null;
		String query;
		ResultSet rs = null;

		if(Utility.isNotNull(username) && Utility.isNotNull(password)){
			try {
				dbConn = ConnectionFactory.createConnection();	
				
				dbConn.setAutoCommit(false);
				
				int affectedRows = 1;
				
				if (pubKey != null){
					query = "UPDATE Users SET pubkey = '" + pubKey + "'WHERE username = '" + username
							+ "' AND password=" + "'" + password + "'";
					updatePubKeyStmt = dbConn.prepareStatement(query);
					affectedRows = updatePubKeyStmt.executeUpdate();
				}

				if(affectedRows > 0){
					query = "SELECT * FROM Users WHERE username = '" + username
							+ "' AND password=" + "'" + password + "'";
					getUserStmt = dbConn.prepareStatement(query);
					rs = getUserStmt.executeQuery(query);
					while (rs.next()) {
						int points = rs.getInt("points");
						pubKey = rs.getString("pubkey");
						user = new User(username, points, pubKey);
					}
				}
				dbConn.commit();
			} catch (Exception e) {

			}finally {
	            DbUtil.close(rs);
	            DbUtil.close(updatePubKeyStmt);
	            DbUtil.close(getUserStmt);
	            DbUtil.close(dbConn);
			}
		}
		
		return user;
	}

	public int sendPoints(String usernameFrom, String usernameTo, int points, String transactionid, String signature) {

		int status = 2;
		String query;
		Connection dbConn = null;
		String pubKey = null;
		PreparedStatement getPublicKeyStmt = null;
		PreparedStatement updateUserFromStmt = null;
		PreparedStatement updateUserToStmt = null;
		PreparedStatement storeTransactionIdStmt = null;
		ResultSet rs = null;
		try {
			dbConn = ConnectionFactory.createConnection();
			
			dbConn.setAutoCommit(false);

			query = "SELECT pubkey FROM Users WHERE username = '" + usernameFrom + "'";
			getPublicKeyStmt = dbConn.prepareStatement(query);
			rs = getPublicKeyStmt.executeQuery(query);
			while (rs.next()) {
				pubKey = rs.getString("pubkey");
			}
			
			if (validSignature(pubKey, usernameTo, points, transactionid, signature)){
								
				query = "UPDATE Users SET points = points -"+ points +" WHERE username = '" + usernameFrom + "' AND points - '"+ points +"' > 0 AND  '"+ transactionid +"' NOT IN (SELECT * FROM Transactions)";
				updateUserFromStmt = dbConn.prepareStatement(query);
	
				query = "UPDATE Users SET points = points + "+ points +" WHERE username = '" + usernameTo + "' AND '"+ transactionid +"' NOT IN (SELECT * FROM Transactions)";
				updateUserToStmt = dbConn.prepareStatement(query);
				
				query = "INSERT INTO Transactions(id) values('"+ transactionid + "')";
				storeTransactionIdStmt = dbConn.prepareStatement(query);
				
				int records = updateUserFromStmt.executeUpdate();	
				
				if (records > 0) { //When points we re sucessfully subtracted
					updateUserToStmt.executeUpdate();
					storeTransactionIdStmt.executeUpdate();
					status = 0;
				}
				else{ // insuficcient points
					status = 1;
				}
			}
			
			dbConn.commit();
		}catch (SQLException e ) {
			status = 2;
	        if (dbConn != null) {
	            try {
	                System.err.println("Transaction is being rolled back");
	                dbConn.rollback();
	            } catch(SQLException excep) {
	            }
	        }
	    } finally {
            DbUtil.close(updateUserFromStmt);
            DbUtil.close(updateUserToStmt);
            DbUtil.close(getPublicKeyStmt);
            DbUtil.close(storeTransactionIdStmt);
            DbUtil.close(rs);
            DbUtil.close(dbConn);
		}
		return status;
	}

	@SuppressWarnings("deprecation")
	private boolean validSignature(String pubKey, String usernameTo, int points, String transactionid, String signature) {
		byte[] content = (usernameTo + points + transactionid).getBytes();
		if (pubKey != null && signature != null){
			signature = URLDecoder.decode(signature).replaceAll(" ", "+").replaceAll("\n", "");
			return RSATools.validateSignature(content, RSATools.decode(signature), RSATools.decodePublicKey(pubKey));
		}
		else{
			return true;
		}
	}

}
