package pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions;

public class BookingBikeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookingBikeException(String msg){
		super(msg);
	}
}
