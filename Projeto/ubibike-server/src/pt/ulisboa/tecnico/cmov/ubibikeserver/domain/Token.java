package pt.ulisboa.tecnico.cmov.ubibikeserver.domain;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import pt.ulisboa.tecnico.cmov.ubibikeserver.services.TokenManager;

public class Token {
	
	private String username;
	
	private String token;
	
	private Date timestamp;
	
	public Token(String username, String token, Date timestamp){
		this.username = username;
		this.token = token;
		this.timestamp = timestamp;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getToken(){
		return this.token;
	}
	
	public void setToken(String token){
		this.token = token;
	}

	public boolean hasExpired() {
		Date currentDate = new Date();

		long duration = currentDate.getTime() - timestamp.getTime();
		long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);

		return diffInHours >= TokenManager.TOKEN_VALIDITY_TIME;
	}
}
