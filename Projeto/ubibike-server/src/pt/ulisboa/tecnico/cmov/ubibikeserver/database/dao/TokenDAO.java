package pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.ConnectionFactory;
import pt.ulisboa.tecnico.cmov.ubibikeserver.database.DbUtil;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

public class TokenDAO {
	
	private Connection dbConnection;
	private Statement statement;
	
	public Token getToken(String sessionToken) throws SQLException, TokenNotFoundException, ExpiredTokenException{
		Token token = null;
		ResultSet rs = null;
		try {
			dbConnection = ConnectionFactory.createConnection();			
			statement = dbConnection.createStatement();
			
			String query = "SELECT username, token, timestamp FROM Users WHERE token='" + sessionToken + "'";

			rs = statement.executeQuery(query);
			
			while (rs.next()) {
				String username = rs.getString("username");
				String tokenValue = rs.getString("token");
				Date timestamp = new Date(rs.getTimestamp("timestamp").getTime());
				
				token = new Token(username, tokenValue, timestamp);
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(statement);
            DbUtil.close(dbConnection);
		}
		return token;
	}
	


	public boolean insert( String username, String token) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			dbConn = ConnectionFactory.createConnection();
			stmt = dbConn.createStatement();
			
			String query = "UPDATE Users Set token='" + token + "', timestamp=CURRENT_TIMESTAMP  Where username='"+username+"'";
			
			int records = stmt.executeUpdate(query);
			
			//When record is successfully inserted
			if (records > 0) {
				insertStatus = true;
			}
		} finally {
            DbUtil.close(rs);
            DbUtil.close(stmt);
            DbUtil.close(dbConn);
		}
		return insertStatus;
	}

}
