package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.ConnectionFactory;

@Path("/register")
public class RegisterService {

	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8888/ubibike-server/register/doregister?mail=paulo&username=paulo&password=1234
	public String doLogin(@QueryParam("email") String email, @QueryParam("username") String uname, @QueryParam("password") String pwd){
		String response = "";
		System.out.println("|Register| Name: " + uname + " Password: " + pwd);
		int retCode = registerUser(email, uname, pwd);
		if(retCode == 0){
			response = Utility.constructJSON("RegisterService",true);
		}else if(retCode == 1){
			response = Utility.constructJSON("RegisterService",false, "error_code", ErrorCodes.USER_ALREADY_REGISTRED.getCode(), "error_msg", ErrorCodes.USER_ALREADY_REGISTRED.getMessage());
		}else if(retCode == 2){
			response = Utility.constructJSON("RegisterService",false, "error_code", ErrorCodes.SPECIAL_CHARS_FOUND.getCode(), "error_msg", ErrorCodes.SPECIAL_CHARS_FOUND.getMessage());
		}else if(retCode == 3){
			response = Utility.constructJSON("RegisterService",false, "error_code", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
				
	}
	
	private int registerUser(String email, String uname, String pwd){
		int result = 3;
		if(Utility.isNotNull(uname) && Utility.isNotNull(pwd)){
			try {
				if(ConnectionFactory.insertUser(email, uname, pwd)){
					result = 0;
				}
			} catch(SQLException sqle){
				sqle.printStackTrace();
				//When Primary key violation occurs that means user is already registered
				if(sqle.getErrorCode() == 1062){
					result = 1;
				} 
				//When special characters are used in email,username or password
				else if(sqle.getErrorCode() == 1064){
					result = 2;
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				result = 3;
			}
		}else{
			result = 3;
		}
			
		return result;
	}
	
}
