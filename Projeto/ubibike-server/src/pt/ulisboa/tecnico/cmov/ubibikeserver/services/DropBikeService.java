package pt.ulisboa.tecnico.cmov.ubibikeserver.services;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import pt.ulisboa.tecnico.cmov.ubibikeserver.database.dao.StationDAO;
import pt.ulisboa.tecnico.cmov.ubibikeserver.domain.Token;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.BookingBikeException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.ExpiredTokenException;
import pt.ulisboa.tecnico.cmov.ubibikeserver.exceptions.TokenNotFoundException;

@Path("/dropbike")
public class DropBikeService {

	@GET
	@Produces(MediaType.APPLICATION_JSON) 
	// http://localhost:8080/ubibike-server/dropbike?stationid=station-3
	public String dropBike(@QueryParam("stationid") String stationId, @HeaderParam("Authorization") String sessionToken) {
		String response;
		try{
			Token token = TokenManager.getToken(sessionToken);
			System.out.println("|Drop Bike| User: " + token.getUsername() + " Station: " + stationId);
			dropBikeUpdate(token.getUsername(), stationId);
			response = Utility.constructJSON("DropBikeService", true);
			
		}
		catch (BookingBikeException e){
			response = Utility.constructJSON("DropBikeService", false, "error_msg", ErrorCodes.BOOK_BIKE_FAILED.getCode(), "error_msg", ErrorCodes.BOOK_BIKE_FAILED.getMessage());
		}
		catch (TokenNotFoundException e){
			response = Utility.constructJSON("DropBikeService", false, "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getCode(), "error_msg", ErrorCodes.TOKEN_NOT_FOUND.getMessage());
		}
		catch (ExpiredTokenException e){
			response = Utility.constructJSON("DropBikeService", false, "error_msg", ErrorCodes.TOKEN_EXPIRED.getCode(), "error_msg", ErrorCodes.TOKEN_EXPIRED.getMessage());
		}
		catch(Exception e){
			response = Utility.constructJSON("DropBikeService", false, "error_msg", ErrorCodes.OPERATION_FAILED.getCode(), "error_msg", ErrorCodes.OPERATION_FAILED.getMessage());
		}
		return response;
	}	

	private void dropBikeUpdate(String username, String stationId) throws SQLException, BookingBikeException {
		boolean sucessfullUpdate = new StationDAO().dropBike(username, stationId);
		if (!sucessfullUpdate)
			throw new BookingBikeException("Station doesn't exist or has no bikes to drop");
	}
}
