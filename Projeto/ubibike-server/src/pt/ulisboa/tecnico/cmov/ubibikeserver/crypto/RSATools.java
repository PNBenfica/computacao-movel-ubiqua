package pt.ulisboa.tecnico.cmov.ubibikeserver.crypto;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class RSATools {

	public static KeyPair generateKeys() {

		try {

			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

			keyGen.initialize(1024, random);
			KeyPair keys = keyGen.generateKeyPair();
			return keys;

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] generateHash(byte[] array) {
		try {

			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(array);
			byte[] hash = sha.digest();
			return hash;

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] signContent(byte[] content, PrivateKey privKey) {

		try {

			Signature dsaForSign = Signature.getInstance("SHA1withRSA");
			dsaForSign.initSign(privKey);
			dsaForSign.update(content);
			byte[] signature = dsaForSign.sign();
			return signature;

		} catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return content;

	}

	public static boolean validateSignature(byte[] data, byte[] signature, PublicKey publicKey) {
		try {
			byte[] dataHash = generateHash(data);
			Signature dsaForVerify = Signature.getInstance("SHA1withRSA");
			dsaForVerify.initVerify(publicKey);
			dsaForVerify.update(dataHash);
			return dsaForVerify.verify(signature);
		} catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static byte[] readFileBytes(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	public static PrivateKey readPrivateKey(String filename)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

	public static void writePrivateKey(String filename, PrivateKey privKey)
			throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] privKeyEncoded = privKey.getEncoded();
		FileOutputStream privFos = new FileOutputStream(filename);
		privFos.write(privKeyEncoded);
		privFos.close();
	}

	public static String encodeToString(byte[] array) {
		return Base64.getEncoder().encodeToString(array);
	}

	public static PublicKey decodePublicKey(String pubKey) {
		try {
			return KeyFactory.getInstance("RSA")
					.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(pubKey)));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static byte[] decode(String s) {
		return Base64.getDecoder().decode(s);
	}

}
