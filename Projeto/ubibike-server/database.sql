/* 
 mysql -u ist175449 -p
 saxh1750 
 use ist175449;
*/

DROP TABLE IF EXISTS `Coordinates`;
DROP TABLE IF EXISTS `Trajectories`;
DROP TABLE IF EXISTS `Bikes`;
DROP TABLE IF EXISTS `Users`;
DROP TABLE IF EXISTS `Stations`;
DROP TABLE IF EXISTS `Transactions`;

CREATE TABLE IF NOT EXISTS `Users` (
  `mail` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `points` int DEFAULT 0,
  `token` varchar(50) DEFAULT NULL,
  `pubkey` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
);

CREATE TABLE IF NOT EXISTS `Trajectories` (
  `trajectory_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `distance` double NOT NULL,
  `trajetory_points` int NOT NULL,
  FOREIGN KEY (`username`) REFERENCES Users(`username`),
  PRIMARY KEY (`trajectory_id`)
);

CREATE TABLE IF NOT EXISTS `Coordinates` (
  `trajectory_id` int NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  FOREIGN KEY (`trajectory_id`) REFERENCES Trajectories(`trajectory_id`)
);

CREATE TABLE IF NOT EXISTS `Stations` (
  `station_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`station_id`)
);

CREATE TABLE IF NOT EXISTS `Bikes` (
  `bike_id` varchar(50) NOT NULL,
  `station_id` varchar(50) NOT NULL,
  `booked_by_user` varchar(50),
  FOREIGN KEY (`station_id`) REFERENCES Stations(`station_id`),
  FOREIGN KEY (`booked_by_user`) REFERENCES Users(`username`),
  PRIMARY KEY (`bike_id`)
);

CREATE TABLE IF NOT EXISTS `Transactions` (
  `id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO Users(mail, username, password, points, token) VALUES("alice@gmail.com", "Alice", "passA", 100, "token1");
INSERT INTO Users(mail, username, password, points, token) VALUES("bob@gmail.com", "Bob", "passB", 0, "token2");

INSERT INTO Trajectories(username, date, distance, trajetory_points) VALUES ("Alice", "18/05/2016 15:12", 2700, 50);
INSERT INTO Trajectories(username, date, distance, trajetory_points) VALUES ("Alice", "19/05/2014 08:25", 2700, 50);

INSERT INTO Stations(station_id, name, location, latitude, longitude) values("Saldanha", "Saldanha Station","Saldanha", 38.7262, -9.14944);
INSERT INTO Stations(station_id, name, location, latitude, longitude) values("Rossio", "Restauradores Station","Restauradores", 38.715, -9.14108);
INSERT INTO Stations(station_id, name, location, latitude, longitude) values("Alameda", "Alameda Station","Alameda", 38.736946, -9.136567);


INSERT INTO Bikes(bike_id, station_id) values("BK1", "Rossio");
INSERT INTO Bikes(bike_id, station_id) values("BK2", "Saldanha");
INSERT INTO Bikes(bike_id, station_id) values("BK3", "Saldanha");
INSERT INTO Bikes(bike_id, station_id) values("BK4", "Alameda");
INSERT INTO Bikes(bike_id, station_id) values("BK5", "Alameda");



INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7338, -9.14417);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73362, -9.14419);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73344, -9.14432);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73326, -9.14457);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73322, -9.14472);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73323, -9.145);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73325, -9.14511);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73314, -9.14524);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73285, -9.14542);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73217, -9.14585);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73177, -9.14607);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.73036, -9.1469);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72908, -9.14767);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72825, -9.14817);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72723, -9.1488);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7262, -9.14944);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72592, -9.1496);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72566, -9.14966);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72561, -9.14959);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72555, -9.14954);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72547, -9.14948);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72534, -9.14944);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7252, -9.14945);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72512, -9.14948);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72506, -9.14952);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.725, -9.14957);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72471, -9.14949);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72458, -9.14944);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72423, -9.14915);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7233, -9.14829);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72266, -9.14773);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72226, -9.14738);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72112, -9.14638);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.72018, -9.14556);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7196, -9.14505);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71944, -9.14492);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71863, -9.14422);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71827, -9.14388);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7179, -9.14356);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71749, -9.14321);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71669, -9.1425);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71644, -9.14221);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71532, -9.14113);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.715, -9.14108);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7147, -9.14076);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71463, -9.14062);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71458, -9.14047);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71449, -9.14011);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71442, -9.13997);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71438, -9.1399);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71443, -9.13984);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71447, -9.13976);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71452, -9.13955);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.7145, -9.1394);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71446, -9.13933);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(1, 38.71436, -9.13924);

INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71423, -9.13919);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71441, -9.13927);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71448, -9.13936);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71451, -9.13945);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71451, -9.13961);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71445, -9.13981);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71441, -9.13988);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71438, -9.1399);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71446, -9.14004);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71452, -9.14022);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71463, -9.14061);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71465, -9.14068);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71472, -9.14078);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.715, -9.14108);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71622, -9.14187);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71655, -9.14238);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71707, -9.14283);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71762, -9.14332);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71801, -9.14366);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71827, -9.14389);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71892, -9.14447);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71948, -9.14496);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.71971, -9.14514);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72037, -9.14573);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72158, -9.14678);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72201, -9.14716);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72262, -9.14769);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72317, -9.14818);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72420, -9.14900);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72452, -9.14942);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72467, -9.14927);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72508, -9.14901);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72531, -9.149);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72561, -9.14912);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72585, -9.14931);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.7262, -9.14944);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72723, -9.1488);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72825, -9.14817);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.72908, -9.14767);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73036, -9.1469);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73177, -9.14607);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73249, -9.14565);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73307, -9.14528);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73314, -9.14524);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73325, -9.14511);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73323, -9.145);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73322, -9.14472);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73326, -9.14457);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73344, -9.14432);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.73362, -9.14419);
INSERT INTO Coordinates(trajectory_id, latitude, longitude) values(2, 38.7338, -9.14417);


SELECT * FROM Users;
SELECT * FROM Trajectories;
SELECT * FROM Stations;
SELECT * FROM Bikes;
SELECT * FROM Coordinates;
SELECT * FROM Transactions;