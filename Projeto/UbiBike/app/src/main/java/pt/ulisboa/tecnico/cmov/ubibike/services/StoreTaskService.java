package pt.ulisboa.tecnico.cmov.ubibike.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;

import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by paulo on 10/05/2016.
 */
public class StoreTaskService {


    public static void store(Activity activity, String url){
        Log.d("StoreTasks", "storing-> " + url);
        writeToFile(activity, url + "\n");
    }

    private static void writeToFile(Activity activity, String content){

        SharedPreferences sharedPreferences = activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        String filename = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "") + ".txt";

        Context context = activity.getApplicationContext();
        try {
            FileWriter out = new FileWriter(new File(context.getFilesDir(), filename), true);
            out.write(content);
            out.close();
        } catch (Exception e) {
        }
    }

    public static void clearAllTasks(Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        String filename = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "") + ".txt";

        Context context = activity.getApplicationContext();
        File dir = context.getFilesDir();
        File file = new File(dir, filename);
        file.delete();
    }
}
