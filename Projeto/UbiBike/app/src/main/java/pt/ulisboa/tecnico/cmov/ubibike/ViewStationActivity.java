package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pt.ulisboa.tecnico.cmov.ubibike.services.remote.BookBikeService;

public class ViewStationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private String stationId;
    private String stationName;
    private String stationLocation;
    private int stationLogo;
    private int stationAvailableBikes;
    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stations);

        setTitleBar();

        setStationInfo(getIntent());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setTitleBar() {
        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Book Bike");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.bike);
    }

    private void setStationInfo(Intent intent) {
        this.stationId = intent.getStringExtra("station-id");
        this.stationName = intent.getStringExtra("station-name");
        this.stationLocation = intent.getStringExtra("station-location");
        this.stationLogo = intent.getIntExtra("station-image", 0);
        this.stationAvailableBikes = intent.getIntExtra("station-available-bikes", 0);
        this.latitude = intent.getDoubleExtra("station-latitude", 0);
        this.longitude = intent.getDoubleExtra("station-longitude", 0);


        TextView usernameTextView = (TextView) findViewById(R.id.stationName);
        usernameTextView.setText(stationName);

        TextView locationTextView = (TextView) findViewById(R.id.station_location);
        locationTextView.setText("Location: " + stationLocation);

        TextView availableBikesTextView = (TextView) findViewById(R.id.available_bikes);
        availableBikesTextView.setText("Available bikes: " + stationAvailableBikes);

        ImageView stationImageView = (ImageView) findViewById(R.id.station_logo);
        stationImageView.setImageResource(stationLogo);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng stationCoords = new LatLng(this.latitude, this.longitude);
        mMap.addMarker(new MarkerOptions().position(stationCoords).title("Station is here"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(stationCoords, 16));
    }

    public void bookBike(View view){
        LatLng stationCoords = new LatLng(this.latitude, this.longitude);
        BookBikeService bookBikeService = new BookBikeService(this, this.stationId,stationCoords);
        bookBikeService.execute();
    }
}
