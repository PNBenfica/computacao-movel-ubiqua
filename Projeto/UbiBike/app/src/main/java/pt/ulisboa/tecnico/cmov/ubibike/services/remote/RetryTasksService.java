package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.cmov.ubibike.services.StoreTaskService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RemoteServices;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by paulo on 10/05/2016.
 */
public class RetryTasksService {

    private Activity activity;

    public RetryTasksService(Activity activity){
        this.activity = activity;
    }

    public void execute() {
        List<String> tasks = loadTasks(); // load the tasks from the file
        StoreTaskService.clearAllTasks(activity);
        for (String url : tasks) {
            retryTask(url);
        }
    }

    private List<String> loadTasks() {

        List<String> tasks = new ArrayList<>();

        try {

            SharedPreferences sharedPreferences = activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
            String filename = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "") + ".txt";

            Context context = activity.getApplicationContext();
            String line;

            BufferedReader in = new BufferedReader(new FileReader(new File(context.getFilesDir(), filename)));
            while ((line = in.readLine()) != null){
                tasks.add(line);
            }

            in.close();
        }
        catch (Exception e) {
        }
        return tasks;
    }


    private void retryTask(final String url) {
        Log.d("StoreTasks", "Retrying-> " + url);
        RemoteServices.get(activity, url, true, true, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("status")) {

                    }
                    else{
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED ) {
                            renewToken();
                        }
                    }
                } catch (JSONException e) {
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                StoreTaskService.store(activity, url);
            }
        });
    }


    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(activity);
        renewTokenService.execute();
    }
}
