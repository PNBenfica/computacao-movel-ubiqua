package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.SendPointsActivity;
import pt.ulisboa.tecnico.cmov.ubibike.services.GetTokenService;
import pt.ulisboa.tecnico.cmov.ubibike.services.StoreTaskService;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by goncaloceia on 28-04-2016.
 */
public class ReceivePointsService {
    // http://localhost:8888/ubibike-server/receivepoints?username=paulo&points=120&transactionid=randomstring

    private String username;
    private String transactionid;
    private int points;
    private Activity activity;
    private String token;

    public ReceivePointsService(String username, int points, String transactionid, Activity activity){
        this.username = username;
        this.points = points;
        this.transactionid = transactionid;
        this.activity = activity;
        GetTokenService getTokenService = new GetTokenService(this.activity);
        getTokenService.execute();
        this.token = getTokenService.response();
    }

    public void execute(){
        final String url =  UrlResources.URL() + "receivepoints/?username=" + username + "&points=" + points + "&transactionid=" + transactionid;

        RemoteServices.get(this.activity, url, true, true, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("status")) {
                        // update points
                    }
                    else{
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED ) {
                            renewToken();
                        }
                    }
                } catch (JSONException e) {
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                StoreTaskService.store(activity, url);
            }
        });
    }


    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(activity);
        renewTokenService.execute();
    }

}
