package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

import pt.ulisboa.tecnico.cmov.ubibike.domain.UserContact;

/**
 * Created by goncaloceia on 14-04-2016.
 */
public class InfoMessage extends Communication{

    private String username;
    private int points;
    private String ipAddress;



    public InfoMessage(String message){
        super(message);
        setType(MessageTypes.USERINFO);
    }

    public InfoMessage(String username, int points, String myIp){
        super(username);
        setType(MessageTypes.USERINFO);
        this.username = username;
        this.points = points;
        this.ipAddress = myIp;
    }

    @Override
    public String generateMessage(){
        return getType() + MessageTypes.DELIMITER +
                MessageTypes.USERNAME + MessageTypes.DELIMITER + this.username + MessageTypes.DELIMITER +
                MessageTypes.POINTS + MessageTypes.DELIMITER + this.points + MessageTypes.DELIMITER +
                MessageTypes.IP + MessageTypes.DELIMITER + this.ipAddress;
    }

}
