package pt.ulisboa.tecnico.cmov.ubibike.wifidirect;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.ulisboa.tecnico.cmov.ubibike.UbiBikeApplication;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.InfoRequestMessage;

/**
 * Created by goncaloceia on 15-04-2016.
 */
public class GroupInfoList implements SimWifiP2pManager.PeerListListener, SimWifiP2pManager.GroupInfoListener {
    private Activity activity;
    private String usernamecontact;

    private SimWifiP2pInfo groupInfo;
    private SimWifiP2pDeviceList devices;
    private Beacons beacons;



    public GroupInfoList(Activity activity, Beacons beaconManager){

        this.activity = activity;
        this.beacons = beaconManager;
    }

    public String UserIp(){
        return this.usernamecontact;
    }

    public String [] ipInRange(SimWifiP2pDeviceList devices,SimWifiP2pInfo groupInfo){
        String response = "";
        String [] args = new String[groupInfo.getDevicesInNetwork().size()];
        int i = 0;
        for (String deviceName : groupInfo.getDevicesInNetwork()) {
            SimWifiP2pDevice device = devices.getByName(deviceName);
            response = ((device == null)) ? "?" : device.getVirtIp();
            args[i++] = response;
        }
        return args;

    }

    @Override
    public void onPeersAvailable(SimWifiP2pDeviceList peers) {
        StringBuilder peersStr = new StringBuilder();
        this.beacons.onPeersAvailable(peers);
    }

    @Override
    public void onGroupInfoAvailable(SimWifiP2pDeviceList devices,SimWifiP2pInfo groupInfo) {
        this.groupInfo = groupInfo;
        this.devices = devices;
        String[] response = ipInRange(devices, groupInfo);
        String ip = getIp(devices, groupInfo);
        WifiDirectManager wifiDirectManager =  ((UbiBikeApplication) this.activity.getApplication()).getWifiDirectManager();
        wifiDirectManager.resetUserContacts();
        wifiDirectManager.updateUsersContacts();
        for(String contact : response) {
            InfoRequestMessage infoRequestMessage = new InfoRequestMessage(ip);
            MessengerOutgoingTask messengerOutgoingTask = new MessengerOutgoingTask(activity, contact);
            Toast.makeText(this.activity,infoRequestMessage.generateMessage(), Toast.LENGTH_LONG).show();

            messengerOutgoingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, infoRequestMessage.generateMessage());
        }
    }

    public String getIp(SimWifiP2pDeviceList devices,SimWifiP2pInfo groupInfo){
        String deviceName = groupInfo.getDeviceName();
        SimWifiP2pDevice device = devices.getByName(deviceName);
        String ip = device.getVirtIp();
        return ip;
    }

    public SimWifiP2pInfo getGroupInfo(){
        return this.groupInfo;
    }

    public SimWifiP2pDeviceList getDevices(){
        return this.devices;
    }
}