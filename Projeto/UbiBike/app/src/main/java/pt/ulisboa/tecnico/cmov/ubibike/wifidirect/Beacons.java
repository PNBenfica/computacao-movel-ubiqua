package pt.ulisboa.tecnico.cmov.ubibike.wifidirect;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.ulisboa.tecnico.cmov.ubibike.GPSListener;
import pt.ulisboa.tecnico.cmov.ubibike.R;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Station;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.DropBikeService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RemoteServices;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.UrlResources;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.MessageTypes;

/**
 * Created by goncaloceia on 30-04-2016.
 */
public class Beacons {


    private Activity activity;
    private SimWifiP2pDeviceList peersBeacons;
    private boolean picked;
    private String bikeId;
    private String stationId;
    private String lastStationId;


    private boolean canDepart;
    private boolean arrive;

    public Beacons(Activity activity){
        this.bikeId = null;
        this.stationId = null;
        this.lastStationId = null;
        this.activity = activity;
        this.canDepart = false;
        this.picked = false;
        this.arrive = false;

    }

    public boolean isBeacon(String deviceName){  return deviceName.contains(MessageTypes.BEACON);  }

    public boolean isStation(String deviceName){ return deviceName.contains(MessageTypes.BEACONSTATION);}

    public boolean bikeInRange(){
        if(this.peersBeacons == null)
            return false;

        for (SimWifiP2pDevice device : this.peersBeacons.getDeviceList()) {
            if (isBeacon(device.deviceName)) {
                if(device.deviceName.equals(this.bikeId)){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean stationInRange(){
        if(this.peersBeacons == null)
            return false;

        for (SimWifiP2pDevice device : this.peersBeacons.getDeviceList()) {
            if (isStation(device.deviceName)) {
                if(device.deviceName.equals(this.stationId))
                    return true;
                }
        }
        return false;
    }

    public boolean isAnyStationInRange(){
        if(this.peersBeacons == null)
            return false;

        for (SimWifiP2pDevice device : this.peersBeacons.getDeviceList()) {
            if (isStation(device.deviceName)) {
               this.lastStationId = device.deviceName;
               return true;
            }
        }
        return false;
    }

    public void onPeersAvailable(SimWifiP2pDeviceList peers) {

        this.peersBeacons = peers;
        boolean anyStation = isAnyStationInRange();
        boolean bikeRange = bikeInRange();
        if(!anyStation && !bikeRange && this.picked)
            this.arrive = true;
    }

    public void setBikeId(String bikeid){
        this.bikeId = bikeid;
    }

    public void setStationId(String stationId){ this.stationId = stationId;}

    public void setCanDepart(boolean canDepart){ this.canDepart = canDepart;}

    public void setPicked(boolean picked){ this.picked = picked;}

    public boolean departed(){
        Log.d("Bikes", "" + this.bikeInRange() + " "  + this.stationInRange() + " "  + this.canDepart );

        if(this.bikeInRange() && this.stationInRange() && this.canDepart){
            this.canDepart = false;
            this.picked = true;
            Toast.makeText(activity.getApplicationContext(),"I have the " + this.bikeId, Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    public boolean arrived() {
        return this.arrive;
    }

    public String getBikeId() {
        return this.bikeId;
    }
    public String getStationId(){ return this.stationId;}
    public String getLastStationId(){ return  this.lastStationId;}

    public void drop(GPSListener gpsListener) {
        DropBikeService dropBikeService = new DropBikeService(this.activity,this.bikeId,this.lastStationId,gpsListener);
        dropBikeService.execute();
    }

    public void setArrive(boolean arrive) {
        this.arrive = arrive;
    }
}
