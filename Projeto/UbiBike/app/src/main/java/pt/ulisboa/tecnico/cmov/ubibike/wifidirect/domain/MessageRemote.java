package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

import java.util.Date;

/**
 * Created by goncaloceia on 14-04-2016.
 */
public class MessageRemote extends Communication{

    private Date date;
    private String username;
    private String message;

    public MessageRemote(String message){
        super(message);
        setType(MessageTypes.USERMESSAGE);
    }

    public MessageRemote(String username, String message, Date date){
        super(username);
        setType(MessageTypes.USERMESSAGE);
        this.username = username;
        this.message = message;
        this.date = date;
    }

    @Override
    public String generateMessage(){
        return getType() + MessageTypes.DELIMITER +
                this.username + MessageTypes.DELIMITER +
                this.date.toString() + MessageTypes.DELIMITER +
                this.message ;
    }

}
