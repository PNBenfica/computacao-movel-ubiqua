package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.GPSListener;
import pt.ulisboa.tecnico.cmov.ubibike.UbiBikeApplication;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.Beacons;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

/**
 * Created by goncaloceia on 30-04-2016.
 */
public class DropBikeService {

    // http://localhost:8080/ubibike-server/bookbike?stationid=3

    private Activity activity;
    private String bikeid;
    private GPSListener gpsListener;
    private String stationId;

    public DropBikeService(Activity activity, String bikeid, String stationId, GPSListener gpsListener){
        this.activity = activity;
        this.bikeid = bikeid;
        this.gpsListener = gpsListener;
        this.stationId = stationId;

    }
//dropbike?stationid=station-3
    public void execute(){
        RemoteServices.get(this.activity, UrlResources.URL() + "dropbike?stationid=" + this.stationId, true, true, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        WifiDirectManager wifiDirectManager =  ((UbiBikeApplication) activity.getApplication()).getWifiDirectManager();
                        Beacons beaconManager = wifiDirectManager.getBeaconManager();
                        beaconManager.setBikeId(null);
                        beaconManager.setCanDepart(false);
                        beaconManager.setPicked(false);
                        gpsListener.endTrajectory();
                        Toast.makeText(activity.getApplicationContext(),"The " + bikeid + " has been drooped", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED ) {
                            renewToken();
                        }
                        else
                            Toast.makeText(activity.getApplicationContext(), "Unable to drop bike", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(activity.getApplicationContext(), "Unable to drop bike", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(activity.getApplicationContext(), "Unable to drop bike", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(activity);
        renewTokenService.execute();
    }

}
