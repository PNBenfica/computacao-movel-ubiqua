package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

/**
 * Created by goncaloceia on 30-03-2016.
 */
public class ErrorCodes {


    public static final Integer OPERATION_FAILED = 1501; // Generic error

    public static final Integer TOKEN_NOT_FOUND = 1502; // Token provided is not associated with any client
    public static final Integer TOKEN_EXPIRED = 1503;  // Token has expired (expiration time defined in TokenManager.class)

    public static final Integer USER_ALREADY_REGISTRED = 1504;  // User is already registred in the platform
    public static final Integer SPECIAL_CHARS_FOUND = 1505;  // String provided has illegal chars
    public static final Integer INVALID_CREDENTIALS = 1506;  // Credentials provided are invalid

    public static final Integer STATION_NOT_FOUND = 1507; // Station doesn't exist

    public static final Integer BOOK_BIKE_FAILED = 1508; // Station doesn't exist or has no bikes to book

    public static final Integer SEND_POINTS_ERROR = 1509; // Station doesn't exist or has no bikes to book
}
