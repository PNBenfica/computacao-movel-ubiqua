package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.UbiBikeApplication;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.Beacons;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

/**
 * Created by goncaloceia on 29-04-2016.
 */
public class BookBikeService {

    // http://localhost:8080/ubibike-server/bookbike?stationid=3

    private Activity activity;

    private String stationId;
    private LatLng stationCoord;

    public BookBikeService(Activity activity, String stationId, LatLng stationCoords){
        this.activity = activity;
        this.stationId = stationId;
        this.stationCoord = stationCoords;
    }

    public void execute(){
        RemoteServices.get(this.activity, UrlResources.URL() + "bookbike/?stationid=" + this.stationId , true, false, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                       // Toast.makeText(activity.getApplicationContext(), obj.getString("bike-id"), Toast.LENGTH_LONG).show();
                        String bikeid = obj.getString("bike-id");
                        WifiDirectManager wifiDirectManager =  ((UbiBikeApplication) activity.getApplication()).getWifiDirectManager();
                        Beacons beaconManager = wifiDirectManager.getBeaconManager();
                        beaconManager.setBikeId(bikeid);
                        beaconManager.setStationId(stationId);
                        beaconManager.setCanDepart(true);
                        beaconManager.setArrive(false);
                        beaconManager.setPicked(false);
                        Toast.makeText(activity.getApplicationContext(),"The " + bikeid + " has been booked", Toast.LENGTH_LONG).show();
                        wifiDirectManager.updateInRangeRequest();
                    }
                    // Else display error message
                    else {
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED ) {
                            renewToken();
                        }
                        else
                            Toast.makeText(activity.getApplicationContext(), "Unable to book bike", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    Toast.makeText(activity.getApplicationContext(), "Unable to book bike", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(activity.getApplicationContext(), "Unable to book bike", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(activity);
        renewTokenService.execute();
    }

}
