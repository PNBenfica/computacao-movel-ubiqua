package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import pt.ulisboa.tecnico.cmov.ubibike.AppStatus;
import pt.ulisboa.tecnico.cmov.ubibike.services.GetTokenService;
import pt.ulisboa.tecnico.cmov.ubibike.services.StoreTaskService;

/**
 * Created by paulo on 28/03/2016.
 */
public class RemoteServices {

    // needAutorizathionHeader - if true it must append token to the headers
    // resendIfFail - store the tasks if it fails, so that it is resend later
    public static void get(Activity activity, String url, boolean needAutorizathionHeader, boolean resendIfFail, AsyncHttpResponseHandler handler){

        if (AppStatus.isNetworkAvailable(activity.getApplicationContext())){
            AsyncHttpClient client = new AsyncHttpClient();
            if (needAutorizathionHeader){
                GetTokenService tokenService = new GetTokenService(activity);
                tokenService.execute();
                String token = tokenService.response();
                client.addHeader("Authorization", token);
            }
            client.get(url, handler);
        }
        else{
            Toast.makeText(activity.getApplicationContext(), "You need an internet connection to perform this action", Toast.LENGTH_LONG).show();
            if (resendIfFail){
                StoreTaskService.store(activity, url);
            }
        }

    }
}
