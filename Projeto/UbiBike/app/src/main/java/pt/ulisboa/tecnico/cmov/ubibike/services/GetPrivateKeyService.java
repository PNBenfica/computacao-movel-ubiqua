package pt.ulisboa.tecnico.cmov.ubibike.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.security.PrivateKey;

import pt.ulisboa.tecnico.cmov.ubibike.services.RSATools;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by paulo on 04/05/2016.
 */
public class GetPrivateKeyService {

    private PrivateKey privKey;
    private Activity activity;


    public GetPrivateKeyService(Activity activity){
        this.activity = activity;

    }

    public void execute(){
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        this.privKey = RSATools.decodePrivateKey(sharedPreferences.getString(SharedPreferencesCodes.PRIVKEY, ""));
    }

    public PrivateKey response(){
        return this.privKey;
    }
}
