package pt.ulisboa.tecnico.cmov.ubibike;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import java.lang.reflect.Field;
import java.util.HashMap;

import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.MessengerIncomingTask;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

/**
 * Created by paulo on 13/04/2016.
 */
public class UbiBikeApplication extends Application {

    private GPSListener gpsListener;
    private WifiDirectManager wifiDirectManager;


    public GPSListener getGPSListener() {
        return gpsListener;
    }

    public void setGPSListener(GPSListener gpsListener) {
        this.gpsListener = gpsListener;
    }

    public WifiDirectManager getWifiDirectManager(){
        return wifiDirectManager;
    }

    public void setWifiDirectManager(WifiDirectManager wifiDirectManager){
        this.wifiDirectManager = wifiDirectManager;
    }

}
