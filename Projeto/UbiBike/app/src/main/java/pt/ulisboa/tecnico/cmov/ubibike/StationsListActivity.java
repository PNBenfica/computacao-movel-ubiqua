package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.arrayadapters.StationsArrayAdapter;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Station;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RemoteServices;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.UrlResources;

public class StationsListActivity extends AppCompatActivity {

    private ArrayAdapter<Station> mAdapter = null;
    private ArrayList<Station> stationsList = null;
    private ListView stationsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stations_list);

        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Book Bike - Stations");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.bike);


        getStationsList();

        if (stationsList == null) { // just for testing without server
            stationsList = new ArrayList<Station>();
            stationsList.add(new Station("station-1", "Massama", "Massama", R.drawable.station_logo2, 5, 38.7521532, -9.279468199999997));
        }
    }

    public void getStationsList() {
        RemoteServices.get(this, UrlResources.URL() + "stations/", false, false, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {

                        ArrayList<Station> stations = new ArrayList<Station>();
                        JSONArray jsonStations = obj.getJSONArray("stations");
                        for (int i=0;i<jsonStations.length();i++){
                            JSONObject jsonStation = (JSONObject)jsonStations.get(i);

                            String id = jsonStation.getString("id");
                            String name = jsonStation.getString("name");
                            String location = jsonStation.getString("location");
                            int imageResource = R.drawable.station_logo2;
                            int availableBikes = jsonStation.getInt("availableBikes");
                            double latitude = jsonStation.getDouble("latitude");
                            double longitude = jsonStation.getDouble("longitude");
                            stations.add(new Station(id, name , location, imageResource, availableBikes, latitude, longitude));
                        }
                        populateStationList(stations);
                    }
                    // Else display error message
                    else {
                        Toast.makeText(getApplicationContext(), "Unable to retrieve stations list", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve stations list", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(getApplicationContext(), "Unable to retrieve stations list", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void populateStationList(ArrayList<Station> stations){
        stationsListView = (ListView) findViewById(R.id.stations_list);

        stationsList = stations;

        mAdapter = new StationsArrayAdapter(this, stationsList);

        stationsListView.setAdapter((ListAdapter) mAdapter);

        stationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Station station = stationsList.get(position);
                viewStation(station);
            }
        });
    }

    public void viewStation(Station station){

        Intent intent = new Intent(getApplication(), ViewStationActivity.class);
        intent.putExtra("station-id", station.getId());
        intent.putExtra("station-name", station.getName());
        intent.putExtra("station-location", station.getLocation());
        intent.putExtra("station-image", station.getImage());
        intent.putExtra("station-available-bikes", station.getAvailableBikes());
        intent.putExtra("station-latitude", station.getLatitude());
        intent.putExtra("station-longitude", station.getLongitude());
        startActivity(intent);
    }
}
