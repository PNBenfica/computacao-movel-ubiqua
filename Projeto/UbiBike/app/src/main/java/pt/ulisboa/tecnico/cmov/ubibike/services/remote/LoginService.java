package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import pt.ulisboa.tecnico.cmov.ubibike.HomeActivity;
import pt.ulisboa.tecnico.cmov.ubibike.services.GetPrivateKeyService;
import pt.ulisboa.tecnico.cmov.ubibike.services.RSATools;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by goncaloceia on 04-04-2016.
 */
public class LoginService {

    private String username;
    private String password;
    private PrivateKey privKey = null;
    private PublicKey pubKey = null;
    private String token;
    private int points;
    private Activity activity;

    public LoginService(String username, String password, Activity activity){
        this.username = username;
        this.password = password;
        this.activity = activity;
        if(!hasKeyPairs()) {
            KeyPair keyPair = RSATools.generateKeys();
            this.privKey = keyPair.getPrivate();
            this.pubKey = keyPair.getPublic();
        }
    }

    // returns true if the private key is stored in the shared preferences
    private boolean hasKeyPairs() {
        GetPrivateKeyService getPrivateKeyService = new GetPrivateKeyService(activity);
        getPrivateKeyService.execute();
        return getPrivateKeyService.response() != null;
    }


    public boolean checkCredentials(){
        if(username.equals("") && password.equals("")){
            Toast.makeText(this.activity,"Username and Password Fields Are Empty",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(username.equals("")){
            Toast.makeText(this.activity,"Username Field Is Empty",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(password.equals("")){
            Toast.makeText(this.activity,"Password Field Is Empty",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public void saveCredentials(String username, String password, String token, PrivateKey privKey){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SharedPreferencesCodes.USERNAME, username);
        editor.putString(SharedPreferencesCodes.PASSWORD, password);
        editor.putString(SharedPreferencesCodes.TOKEN, token);
        editor.putString(SharedPreferencesCodes.PRIVKEY, RSATools.encodeToString(privKey.getEncoded()));
        editor.commit();

        GetPrivateKeyService getPrivateKeyService = new GetPrivateKeyService(activity);
        getPrivateKeyService.execute();
        getPrivateKeyService.response();
    }

    public void savePoints(int points){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SharedPreferencesCodes.POINTS, points);
        editor.commit();

    }


    public void login(){
        Log.d("crypto", "" + RSATools.encodeToString(pubKey.getEncoded()));
        Log.d("crypto", "" + URLEncoder.encode(RSATools.encodeToString(pubKey.getEncoded())));
        RemoteServices.get(activity, UrlResources.URL() + "login/?username=" + username + "&password=" + password+"&publicKey="+ URLEncoder.encode(RSATools.encodeToString(pubKey.getEncoded())), false, false, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        String token = obj.getString("token");
                        int points = obj.getInt("points");
                        saveCredentials(username, password, token, privKey);
                        savePoints(points);
                        Intent intent = new Intent(activity, HomeActivity.class);
                        activity.startActivity(intent);
                    }
                    // Else display error message
                    else {
                        Toast.makeText(activity.getApplicationContext(), "Unable to login", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(activity.getApplicationContext(), "Unable to login", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(activity.getApplicationContext(), "Unable to login", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void execute(){
        if(checkCredentials())
            login();
    }

}
