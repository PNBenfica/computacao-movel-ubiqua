package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.HomeActivity;
import pt.ulisboa.tecnico.cmov.ubibike.LoginActivity;

/**
 * Created by goncaloceia on 04-04-2016.
 */
public class RegisterService {
    private String username;
    private String password;
    private String passwordconfirm;
    private Activity activity;

    public RegisterService(String username, String password, String passwordconfirm, Activity activity){
        this.username = username;
        this.password = password;
        this.passwordconfirm = passwordconfirm;
        this.activity = activity;
    }

    public boolean checkCredentials(){
        if(username.equals("") && password.equals("")){
            Toast.makeText(this.activity, "Username and Password Fields Are Empty", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(username.equals("")){
            Toast.makeText(this.activity,"Username Field Is Empty",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(password.equals("")){
            Toast.makeText(this.activity,"Password Field Is Empty",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!password.equals(this.passwordconfirm)) {
            Toast.makeText(this.activity, "Password and Confirm Password doesn't match", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public void register(){
        RemoteServices.get(activity, UrlResources.URL() + "register?mail=" + username + "&username=" + username + "&password=" + password, false, false, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        Toast.makeText(activity.getApplicationContext(), "Sucessfull Register", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        activity.startActivity(intent);
                    }
                    // Else display error message
                    else {
                        Toast.makeText(activity.getApplicationContext(), "" + obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(activity.getApplicationContext(), "Unable to register", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(activity.getApplicationContext(), "Unable to register", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void execute(){
        if(checkCredentials())
            register();
    }

}
