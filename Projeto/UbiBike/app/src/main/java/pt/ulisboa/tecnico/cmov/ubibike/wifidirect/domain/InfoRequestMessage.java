package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

/**
 * Created by goncaloceia on 14-04-2016.
 */
public class InfoRequestMessage extends Communication{

    public InfoRequestMessage(String message){
        super(message);
        setType(MessageTypes.USERINFOREQUEST);
    }
}
