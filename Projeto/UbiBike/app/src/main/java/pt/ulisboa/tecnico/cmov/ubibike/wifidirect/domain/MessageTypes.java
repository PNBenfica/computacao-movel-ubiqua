package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

/**
 * Created by goncaloceia on 13-04-2016.
 */
public class MessageTypes {
    public static final String USERINFO = "userinfo";
    public static final String USERINFOREQUEST = "userinforequest";
    public static final String USERMESSAGE = "usermessage";
    public static final String USERPOINTS = "userpoints";
    public static final String DELIMITER = "@:@";
    public static final int MESSAGEINDEX = 2;
    public static final String USERNAME = "username";
    public static final String POINTS = "points";
    public static final String MESSAGE = "message";
    public static final String IP = "AddressIp";
    public static final String PORT = "10001";
    public static final String PATTERN = "£§§@££§";
    public static final String BEACON = "bike";
    public static final String BEACONDELIMITER = "-";
    public static final String BEACONSTATION = "station";

    public static String Type(String arg){
        String[] splitString = arg.split(MessageTypes.DELIMITER);
        return splitString[0];
    }

    public static String Message(String arg){
        String[] splitString = arg.split(MessageTypes.DELIMITER,MessageTypes.MESSAGEINDEX);
        if(splitString.length > 1)
            return splitString[1];
        return null;
    }

}
