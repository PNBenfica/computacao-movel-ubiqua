package pt.ulisboa.tecnico.cmov.ubibike.wifidirect;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Messenger;

import java.util.ArrayList;
import java.util.TreeMap;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.SimWifiP2pManager.Channel;
import pt.inesc.termite.wifidirect.service.SimWifiP2pService;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketManager;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.ulisboa.tecnico.cmov.ubibike.MessengerChatActivity;
import pt.ulisboa.tecnico.cmov.ubibike.MessengerContactsActivity;
import pt.ulisboa.tecnico.cmov.ubibike.SendPointsActivity;
import pt.ulisboa.tecnico.cmov.ubibike.SendPointsContactsActivity;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Message;
import pt.ulisboa.tecnico.cmov.ubibike.domain.UserContact;

/**
 * Created by goncaloceia on 15-04-2016.
 */
public class WifiDirectManager {


    public static final String TAG = "msgsender";
    /**
     * SimWifiP2p Manager
      */
    private SimWifiP2pManager mManager = null;
    private Channel mChannel = null;
    private Messenger mService = null;
    private boolean mBound = false;
    private SimWifiP2pBroadcastReceiver mReceiver;
    /**
     * ServerSocket for incoming communications
     */
    private MessengerIncomingTask messengerIncomingTask;
    /**
     * Stakeholders Activities
      */
    private MessengerContactsActivity messengerContactsActivity;
    private MessengerChatActivity messengerChatActivity;
    private SendPointsContactsActivity sendPointsContactsActivity;
    private SendPointsActivity sendPointsActivity;
    private Activity activity;

    /**
     * Group Comunication Listener
     */
    private GroupInfoList groupInfoList;
    private Beacons beaconManager;
    /**
     * Users Nearby and their messages
     */
    private ArrayList<UserContact> users;
    private TreeMap<String, ArrayList<Message>> messages;


    public WifiDirectManager(Activity activity){
        this.activity = activity;
        this.beaconManager = new Beacons(this.activity);
        this.users = new ArrayList<UserContact>();
        this.messages = new TreeMap<String, ArrayList<Message>>();
        SimWifiP2pSocketManager.Init(this.activity);
        mReceiver = new SimWifiP2pBroadcastReceiver(this.activity, this);
        this.activity.registerReceiver(mReceiver, getFilter());
        groupInfoList = new GroupInfoList(this.activity,beaconManager);
        wifiOn();
    }

    public Beacons getBeaconManager(){ return this.beaconManager;}


    public IntentFilter getFilter(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_GROUP_OWNERSHIP_CHANGED_ACTION);
        return filter;
    }

    public void setMessengerContactsActivity(MessengerContactsActivity messengerContactsActivity){ this.messengerContactsActivity = messengerContactsActivity;   }

    public void setMessengerChatContactsActivity(MessengerChatActivity messengerChatActivity){  this.messengerChatActivity = messengerChatActivity;  }

    public void setSendPointsContactsActivity(SendPointsContactsActivity sendPointsContactsActivity){ this.sendPointsContactsActivity = sendPointsContactsActivity;}

    public void setSendPointsActivity(SendPointsActivity sendPointsActivity){ this.sendPointsActivity = sendPointsActivity;}

    public void updateUsersContacts(){
        if(messengerContactsActivity != null)
            messengerContactsActivity.update();
        if(sendPointsContactsActivity != null)
            sendPointsContactsActivity.update();
    }

    public void updateUserChat(){
        if(messengerChatActivity == null)
            return;
        messengerChatActivity.update();
    }

    public void updateInRangeRequest(){
        if(mBound){
            mManager.requestPeers(mChannel,groupInfoList);
        }
    }

    public void resetUserContacts(){
        this.users.clear();
    }

    public void onPause() {  this.activity.unregisterReceiver(mReceiver);   }

    public boolean getMBound(){ return this.mBound;  }

    public SimWifiP2pManager getMManager(){ return this.mManager; }

    public Channel getMChannel(){  return this.mChannel; }

    public GroupInfoList getGroupInfoList(){ return this.groupInfoList; }

    public Activity getActivity(){  return this.activity;  }

    public void addUser(UserContact usercontact){
        for(UserContact user_temp : this.users){
            if(user_temp.getName().equals(usercontact.getName()))
                return;
        }
        this.users.add(usercontact);
    }

    public ArrayList<UserContact> getUsersNearBy(){   return this.users;  }

    public SimWifiP2pInfo getGroupInfo(){   return this.groupInfoList.getGroupInfo();   }

    public SimWifiP2pDeviceList getDevices(){   return this.groupInfoList.getDevices();  }

    private void wifiOn(){
        Intent intent = new Intent(this.activity, SimWifiP2pService.class);
        this.activity.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mBound = true;
        messengerIncomingTask = new MessengerIncomingTask(this.activity, this);
        messengerIncomingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void updateGroupInfoRequest() {
        if (mBound) {
            mManager.requestGroupInfo(mChannel, groupInfoList);
        }
    }

    public void addMessage(Message message){
        if(!messages.containsKey(message.getFromName()))
            messages.put(message.getFromName(), new ArrayList<Message>());
        ArrayList<Message> usermessages = messages.get(message.getFromName());
        usermessages.add(message);
        messages.put(message.getFromName(), usermessages);
    }

    public ArrayList<Message> getUserMessages(String username){
        if(!messages.containsKey(username))
            messages.put(username, new ArrayList<Message>());
        return messages.get(username);
    }


    private ServiceConnection mConnection = new ServiceConnection() {
        // callbacks for service binding, passed to bindService()

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mManager = new SimWifiP2pManager(mService);
            mChannel = mManager.initialize(activity.getApplication(), activity.getMainLooper(), null);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mManager = null;
            mChannel = null;
            mBound = false;
        }
    };



}
