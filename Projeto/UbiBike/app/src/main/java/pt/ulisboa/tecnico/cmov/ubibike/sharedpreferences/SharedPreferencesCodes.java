package pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences;

import java.security.SecureRandom;

/**
 * Created by goncaloceia on 30-03-2016.
 */
public class SharedPreferencesCodes {
    public static final String CREDENTIALS = "Credentials" ;
    public static final String USERNAME = "usernamekey";
    public static final String PASSWORD = "passwordkey";
    public static final String TOKEN = "tokenkey";
    public static final String POINTS = "pointskey";
    public static final String PRIVKEY = "privatekey";

    public static String generateTransactionId() {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom random = new SecureRandom();

        StringBuilder sb = new StringBuilder(50);
        for (int i = 0; i < 50; i++)
            sb.append(AB.charAt(random.nextInt(AB.length())));
        return sb.toString();
    }

}
