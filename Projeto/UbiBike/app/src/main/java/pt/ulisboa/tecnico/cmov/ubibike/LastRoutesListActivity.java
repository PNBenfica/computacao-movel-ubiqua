package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.arrayadapters.RoutesArrayAdapter;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Coordinate;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Route;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Station;
import pt.ulisboa.tecnico.cmov.ubibike.services.GetTokenService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.ErrorCodes;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RemoteServices;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RenewTokenService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.UrlResources;

public class LastRoutesListActivity extends AppCompatActivity {

    private ArrayAdapter<Route> mAdapter = null;
    private ArrayList<Route> routesList = null;
    private ListView routesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_routes_list);

        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Last Routes");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.maps);

        routesListView = (ListView) findViewById(R.id.last_routes_list);

        getLastRoutes();
    }

    public void getLastRoutes() {

        RemoteServices.get(this, UrlResources.URL() + "trajectories/", true, false,  new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {

                        ArrayList<Route> routesList = new ArrayList<Route>();
                        JSONArray jsonRoutes = obj.getJSONArray("trajectories");
                        for (int i = 0; i < jsonRoutes.length(); i++) {
                            JSONObject jsonRoute = (JSONObject) jsonRoutes.get(i);

                            String date = jsonRoute.getString("date");
                            int distance = jsonRoute.getInt("distance");
                            int points = jsonRoute.getInt("points");

                            JSONArray jsonCoords = jsonRoute.getJSONArray("coordinates");
                            ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>();
                            for(int j = 0; j < jsonCoords.length(); j++) {
                                double latitude = ((JSONObject) jsonCoords.get(j)).getDouble("latitude");
                                double longitude = ((JSONObject) jsonCoords.get(j)).getDouble("longitude");
                                Coordinate coordinate = new Coordinate(latitude, longitude);
                                coordinates.add(coordinate);
                            }
                            routesList.add(new Route(date, distance, points, coordinates));
                        }
                        populateRoutesList(routesList);
                    }
                    else {
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED ) {
                            renewToken();
                        }
                        else
                            Toast.makeText(getApplicationContext(), "Unable to retrieve last trajectories", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Unable to retrieve last trajectories", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(getApplicationContext(), "Unable to retrieve last trajectories", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(this);
        renewTokenService.execute();
    }


    public void populateRoutesList(ArrayList<Route> routes){
        routesListView = (ListView) findViewById(R.id.last_routes_list);

        routesList = routes;

        mAdapter = new RoutesArrayAdapter(this, routesList);

        routesListView.setAdapter((ListAdapter) mAdapter);

        routesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Route route = routesList.get(position);
                viewRoute(route);
            }
        });
    }

    public void viewRoute(Route route){
        Intent intent = new Intent(getApplication(), ViewRouteActivity.class);
        intent.putExtra("route-date", route.getDate());
        intent.putExtra("route-distance", route.getDistance());
        intent.putExtra("route-points", route.getPointsEarned());
        intent.putParcelableArrayListExtra("route-coordinates", route.getCoordinates());
        startActivity(intent);
    }
}

