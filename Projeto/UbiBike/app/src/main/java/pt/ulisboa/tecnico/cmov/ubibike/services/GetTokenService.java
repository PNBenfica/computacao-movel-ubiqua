package pt.ulisboa.tecnico.cmov.ubibike.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by goncaloceia on 03-04-2016.
 */
public class GetTokenService {

    private String token;
    private Activity activity;


    public GetTokenService(Activity activity){
        this.activity = activity;

    }


    public void execute(){
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        this.token = sharedPreferences.getString(SharedPreferencesCodes.TOKEN, "TokenNotFound");
        Toast.makeText(activity.getApplicationContext(), this.token, Toast.LENGTH_LONG).show();
    }

    public String response(){
        return this.token;
    }


}
