package pt.ulisboa.tecnico.cmov.ubibike.domain;

import java.util.ArrayList;

/**
 * Created by paulo on 28/03/2016.
 */
public class Route {

    private String date;

    private int distance;

    private int pointsEarned;

    private ArrayList<Coordinate> coordinates;

    public Route(String date, int distance, int pointsEarned, ArrayList<Coordinate> coordinates){
        this.date = date;
        this.distance = distance;
        this.pointsEarned = pointsEarned;
        this.coordinates = coordinates;
    }

    public int getPointsEarned() {
        return pointsEarned;
    }

    public int getDistance() {
        return distance;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<Coordinate> getCoordinates() {
        return coordinates;
    }
}
