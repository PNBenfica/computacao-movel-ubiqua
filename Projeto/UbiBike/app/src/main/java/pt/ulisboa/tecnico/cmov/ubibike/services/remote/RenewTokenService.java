package pt.ulisboa.tecnico.cmov.ubibike.services.remote;


import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.HomeActivity;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by goncaloceia on 03-04-2016.
 */
public class RenewTokenService {
    private String username;
    private String password;
    private String token;
    private Activity activity;


    public RenewTokenService(Activity activity){
        this.activity = activity;
        getCredentials();
    }


    public void getCredentials(){
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        this.username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");
        this.password = sharedPreferences.getString(SharedPreferencesCodes.PASSWORD, "PasswordNotFound");
    }

    public void saveCredentials(){
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SharedPreferencesCodes.USERNAME, this.username);
        editor.putString(SharedPreferencesCodes.PASSWORD, this.password);
        editor.putString(SharedPreferencesCodes.TOKEN, this.token);
        editor.commit();
    }

    public void execute(){
        RemoteServices.get(activity, UrlResources.URL() + "login/?username=" + username + "&password=" + password, false, false, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {

                        token = obj.getString("token");
                        saveCredentials();

                        Intent intent = activity.getIntent();
                        activity.finish();
                        activity.startActivity(intent);
                    }
                    // Else display error message
                    else {
                        Toast.makeText(activity.getApplicationContext(), "Unable to renew session token", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(activity.getApplicationContext(), "Unable to renew session token", Toast.LENGTH_LONG).show();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                Toast.makeText(activity.getApplicationContext(), "Unable to renew session token", Toast.LENGTH_LONG).show();
            }
        });
    }



}
