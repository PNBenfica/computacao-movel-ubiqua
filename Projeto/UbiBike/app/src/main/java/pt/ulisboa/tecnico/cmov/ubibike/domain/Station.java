package pt.ulisboa.tecnico.cmov.ubibike.domain;

/**
 * Created by paulo on 27/03/2016.
 */

public class Station {

    private String id;

    private String name;

    private int image;

    private int availableBikes;

    private String location;

    private double latitude;

    private double longitude;

    public Station(String id, String name, String location, int image, int availableBikes, double latitude, double longitude){
        this.id = id;
        this.name = name;
        this.image = image;
        this.location = location;
        this.availableBikes = availableBikes;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public int getImage() {
        return image;
    }

    public int getAvailableBikes() {
        return availableBikes;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
