package pt.ulisboa.tecnico.cmov.ubibike.wifidirect;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketServer;
import pt.ulisboa.tecnico.cmov.ubibike.R;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Message;
import pt.ulisboa.tecnico.cmov.ubibike.domain.UserContact;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.ReceivePointsService;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.InfoMessage;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.MessageTypes;

/**
 * Created by goncaloceia on 13-04-2016.
 */
public class MessengerIncomingTask extends AsyncTask<Void, String, Void> {

    public static final String TAG = "msgsender";

    private Activity activity;
    private SimWifiP2pSocketServer mSrvSocket;
    private MessengerOutgoingTask messengerOutgoingTask;
    private WifiDirectManager wifiDirectManager;
    private String username;
    private ArrayList<String> notifications;

    public MessengerIncomingTask(Activity activity, WifiDirectManager wifiDirectManager){
        super();
        notifications = new ArrayList<>();
        this.activity = activity;
        this.wifiDirectManager = wifiDirectManager;
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        this.username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");
    }

    @Override
    protected Void doInBackground(Void... params) {

        Log.d(TAG, "IncommingCommTask started (" + this.hashCode() + ").");

        try {
            mSrvSocket = new SimWifiP2pSocketServer( Integer.parseInt(MessageTypes.PORT));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (!Thread.currentThread().isInterrupted()) {
            try {
                SimWifiP2pSocket sock = mSrvSocket.accept();

                try {
                    BufferedReader sockIn = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String st = sockIn.readLine();
                    publishProgress(st);
                    sock.getOutputStream().write(("\n").getBytes());
                } catch (IOException e) {
                    Log.d("Error reading socket:", e.getMessage());
                } finally {
                    sock.close();
                }
            } catch (IOException e) {
                Log.d("Error socket:", e.getMessage());
                break;
                //e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    protected void onProgressUpdate(String... values) {

        String type = MessageTypes.Type(values[0]);
        String message = MessageTypes.Message(values[0]);

        Toast.makeText(this.activity,values[0], Toast.LENGTH_LONG).show();
        if(type.equals(MessageTypes.USERINFO)) {
            updateUserInfo(message);
        }
        else if(type.equals(MessageTypes.USERINFOREQUEST)){
            replyUserInfo(message);
        }
        else if(type.equals(MessageTypes.USERMESSAGE)) {
            updateMessage(message);
        }
        else if(type.equals(MessageTypes.USERPOINTS)){
            updatePoints(message);
        }
    }

    public void updatePoints(String message){
        String [] messageinfo = message.split(MessageTypes.DELIMITER);
        String username = messageinfo[0];
        int pointsReceived = Integer.parseInt(messageinfo[1]);
        String transactionid = messageinfo[2];
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        int  userpoints = sharedPreferences.getInt(SharedPreferencesCodes.POINTS, -1);
        int totalpoints  = pointsReceived + userpoints;
        Toast.makeText(this.activity, username + " " + totalpoints + " " + message + " " + pointsReceived, Toast.LENGTH_LONG).show();

        ReceivePointsService receivePointsService = new ReceivePointsService(username, pointsReceived, transactionid, this.activity);
        receivePointsService.execute();
    }


    public void updateMessage(String message){
        String [] messageinfo = message.split(MessageTypes.DELIMITER, MessageTypes.MESSAGEINDEX + 1);
        String userinfo = messageinfo[0];
        Date date = new Date(messageinfo[1]);
        String messagecontent = messageinfo[2];
        Message message1 = new Message(userinfo,messagecontent,false,date);
        this.wifiDirectManager.addMessage(message1);
        this.wifiDirectManager.updateUserChat();

        ActivityManager am = (ActivityManager) this.activity.getSystemService(this.activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        String activityname = taskInfo.get(0).topActivity.getShortClassName();
        activityname = activityname.replace(".", "");
        if(!activityname.equals("MessengerChatActivity")){
            sendNotification(userinfo,messagecontent);
        }
    }

    public void sendNotification(String username, String message){
        int notificationid = this.notifications.indexOf(username);
        if(notificationid == -1){
            this.notifications.add(username);
            notificationid = this.notifications.indexOf(username);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.activity);
        mBuilder.setSmallIcon(R.drawable.bike);
        mBuilder.setContentTitle("Message from: " + username);
        mBuilder.setContentText(message);
        NotificationManager mNotificationManager = (NotificationManager) this.activity.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(notificationid, mBuilder.build());

    }

    public void updateUserInfo(String message){
        UserContact userContact = parseUserInfo(message);
        this.wifiDirectManager.addUser(userContact);
        this.wifiDirectManager.updateUsersContacts();
    }

    public void replyUserInfo(String message){
        String username = this.username;
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        int points = sharedPreferences.getInt(SharedPreferencesCodes.POINTS, -1);
        String deviceName = wifiDirectManager.getGroupInfo().getDeviceName();
        SimWifiP2pDevice device = wifiDirectManager.getDevices().getByName(deviceName);
        String myIp = device.getVirtIp();
        sendMessage(username,points,myIp,message);
    }

    public void sendMessage(String username, int points, String myIp,String ip){
        InfoMessage infoMessage = new InfoMessage(username,points, myIp);

        this.messengerOutgoingTask = new MessengerOutgoingTask(activity,ip);

        this.messengerOutgoingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, infoMessage.generateMessage());
    }

    public UserContact parseUserInfo(String userinfo){
        String [] userInfoTemp = userinfo.split(MessageTypes.DELIMITER);
        String username = "UsernameNotFound";
        int points = -1;
        String ipAddress = "?";

        for(int i = 0; i < userInfoTemp.length;i++){
            if(userInfoTemp[i].equals(MessageTypes.USERNAME))
                username = userInfoTemp[i + 1];
            else if(userInfoTemp[i].equals(MessageTypes.POINTS))
                points = Integer.parseInt(userInfoTemp[i + 1]);
            else if(userInfoTemp[i].equals(MessageTypes.IP))
                ipAddress = userInfoTemp[i + 1];
        }

        UserContact userContact = new UserContact(username, R.drawable.profile_image, points, ipAddress);
        return userContact;
    }
}