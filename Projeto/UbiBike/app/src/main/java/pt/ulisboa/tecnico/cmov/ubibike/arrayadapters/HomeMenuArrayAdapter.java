package pt.ulisboa.tecnico.cmov.ubibike.arrayadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.domain.HomeMenuOption;
import pt.ulisboa.tecnico.cmov.ubibike.R;

/**
 * Created by paulo on 26/03/2016.
 */
public class HomeMenuArrayAdapter  extends ArrayAdapter<HomeMenuOption> {

    private final Context context;
    private final ArrayList<HomeMenuOption> options;

    public HomeMenuArrayAdapter(Context context, ArrayList<HomeMenuOption> options) {
        super(context, R.layout.menu_option, options);
        this.context = context;
        this.options = options;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View optionView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        optionView = inflater.inflate(R.layout.menu_option, parent, false);

        TextView textView = (TextView) optionView.findViewById(R.id.menu_option_text);
        textView.setText(options.get(position).getName());

        ImageView imageView = (ImageView) optionView.findViewById(R.id.menu_option_image);
        imageView.setImageResource(options.get(position).getImage());

        return optionView;
    }
}
