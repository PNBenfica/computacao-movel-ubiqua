package pt.ulisboa.tecnico.cmov.ubibike.arrayadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.R;
import pt.ulisboa.tecnico.cmov.ubibike.domain.UserContact;

/**
 * Created by paulo on 27/03/2016.
 */
public class UserContactArrayAdapter   extends ArrayAdapter<UserContact> {

    private final Context context;
    private final ArrayList<UserContact> contacts;

    public UserContactArrayAdapter(Context context, ArrayList<UserContact> contacts) {
        super(context, R.layout.contact_list_item, contacts);
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View contactListView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        contactListView = inflater.inflate(R.layout.contact_list_item, parent, false);

        TextView usernameTextView = (TextView) contactListView.findViewById(R.id.contact_name);
        usernameTextView.setText(contacts.get(position).getName());

        TextView pointsTextView = (TextView) contactListView.findViewById(R.id.contact_user_points);
        pointsTextView.setText("" + contacts.get(position).getPoints() + " points");

        ImageView imageView = (ImageView) contactListView.findViewById(R.id.contact_image);
        imageView.setImageResource(contacts.get(position).getImage());

        return contactListView;
    }
}