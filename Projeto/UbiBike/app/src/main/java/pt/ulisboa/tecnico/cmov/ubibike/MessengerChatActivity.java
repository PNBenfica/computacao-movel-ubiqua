package pt.ulisboa.tecnico.cmov.ubibike;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import pt.ulisboa.tecnico.cmov.ubibike.arrayadapters.MessagesArrayAdapter;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Message;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.MessengerOutgoingTask;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.MessageRemote;


public class MessengerChatActivity extends Activity implements View.OnClickListener {

    private String chatContactUsername;
    private int chatContactImage;
    private String chatContactIp;
    private ImageButton sendButton;
    private EditText messageText;
    private ListView messageList;
    private MessagesArrayAdapter mAdapter = null;
    private ArrayList<Message> messages = null;
    private WifiDirectManager wifiDirectManager;

    int in_index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger_chat);
        wifiDirectManager =  ((UbiBikeApplication) this.getApplication()).getWifiDirectManager();
        wifiDirectManager.setMessengerChatContactsActivity(this);


        Intent intent = getIntent();
        chatContactUsername = intent.getStringExtra("chat-contact-name");
        chatContactImage = intent.getIntExtra("chat-contact-image", 0);
        chatContactIp = intent.getStringExtra("chat-contact-address");
        setTitleBar(chatContactUsername, chatContactImage);

        sendButton = (ImageButton) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(this);

        messageText = (EditText) findViewById(R.id.messageText);

        messageList = (ListView) findViewById(R.id.messageList);

        // messages = new ArrayList<String>();
        messages = wifiDirectManager.getUserMessages(chatContactUsername);

        // mAdapter = new ArrayAdapter<String>(this, R.layout.mymessage, R.id.mymessageTextView, messages);
        mAdapter = new MessagesArrayAdapter(this, messages);

        messageList.setAdapter((ListAdapter) mAdapter);

    }

    private void setTitleBar(String username, int userImage) {
        TextView nameView = (TextView) findViewById(R.id.contact_name);
        nameView.setText(username);

        ImageView imageView = (ImageView) findViewById(R.id.contact_image);
        imageView.setImageResource(userImage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.sendButton:

                String messString = messageText.getText().toString();
                if (!messString.equals("")) {
                    Date date = new Date();
                    Message message = new Message("", messString, true, date);
                    messages.add(message);
                    mAdapter.notifyDataSetChanged();
                    //sendMessage();

                    SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
                    String username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");
                    MessageRemote messageRemote = new MessageRemote(username,messString,date);
                    MessengerOutgoingTask messengerOutgoingTask = new MessengerOutgoingTask(wifiDirectManager.getActivity(),chatContactIp);
                    messengerOutgoingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, messageRemote.generateMessage());

                    messageText.setText("");

                }

                break;

            default:
                break;
        }
    }

    public void sendMessage() {

        String[] incoming = {"Hey, How's it going?",
                "Ride a lot today?",
                "Make me company down this hill?",
                "Great, let's go!",
                "C'mon you got to keep it up!"};

        if (in_index < incoming.length) {
            Message message = new Message(this.chatContactUsername, incoming[in_index], false,  new Date());
            messages.add(message);
            in_index++;
            mAdapter.notifyDataSetChanged();
        }

    }

    public void update(){
        mAdapter.notifyDataSetChanged();
    }
}
