package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

/**
 * Created by goncaloceia on 04-04-2016.
 */
public class UrlResources {

    //public static final String IP = "192.168.1.13" ;
    public static final String IP = "194.210.222.77" ;
    public static final String PORT = "8080";
    public static final String UBIBIKESERVER = "ubibike-server";

    public static String URL(){
        return "http://" + IP + ":" + PORT  + "/" + UBIBIKESERVER + "/";
    }

}
