package pt.ulisboa.tecnico.cmov.ubibike.domain;

/**
 * Created by paulo on 27/03/2016.
 */
public class UserContact {

    private String name;

    private int image;

    private int points;

    private String virtualIp;

    public UserContact(String name, int image, int points, String virtualIp){
        this.name = name;
        this.image = image;
        this.points = points;
        this.virtualIp = virtualIp;
    }

    public String getName(){ return this.name; }

    public int getImage(){
        return this.image;
    }

    public int getPoints(){
        return this.points;
    }

    public String getVirtualIp(){ return this.virtualIp; }
}
