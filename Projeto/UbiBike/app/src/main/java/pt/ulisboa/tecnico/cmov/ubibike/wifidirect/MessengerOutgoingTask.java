package pt.ulisboa.tecnico.cmov.ubibike.wifidirect;

import android.app.Activity;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.MessageTypes;

/**
 * Created by goncaloceia on 13-04-2016.
 */
public class MessengerOutgoingTask
        extends AsyncTask<String, String, Void> {

    private Activity activity;
    private SimWifiP2pSocket mCliSocket;
    private String usernameip;

    public MessengerOutgoingTask(Activity activity, String usernameip){
        super();
        this.activity = activity;
        this.usernameip = usernameip;
    }

    @Override
    protected Void doInBackground(String... msg) {
        try {
            mCliSocket = new SimWifiP2pSocket(this.usernameip, Integer.parseInt(MessageTypes.PORT));
            mCliSocket.getOutputStream().write((msg[0] + "\n").getBytes());
            BufferedReader sockIn = new BufferedReader(new InputStreamReader(mCliSocket.getInputStream()));
            sockIn.readLine();
            mCliSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCliSocket = null;
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
       // activity.mTextInput().setText("");

    }
}

