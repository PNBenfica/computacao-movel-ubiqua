package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

/**
 * Created by goncaloceia on 14-04-2016.
 */
public abstract class Communication {
    private String type;
    private String message;

    public Communication(String message){
        this.type = null;
        this.message = message;
    }

    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }

    public String generateMessage(){
        return this.type + MessageTypes.DELIMITER + this.message;
    }


}
