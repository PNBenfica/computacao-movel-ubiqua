package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.arrayadapters.UserContactArrayAdapter;
import pt.ulisboa.tecnico.cmov.ubibike.domain.UserContact;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

public class SendPointsContactsActivity extends AppCompatActivity {

    private ArrayAdapter<UserContact> mAdapter = null;
    private ArrayList<UserContact> contactsList = null;
    private ListView contactsListView;
    private WifiDirectManager wifiDirectManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendpoints_contacts);


        wifiDirectManager =  ((UbiBikeApplication) this.getApplication()).getWifiDirectManager();
        wifiDirectManager.setSendPointsContactsActivity(this);

        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Send Points");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.gift);
        TextView textViewPoints = (TextView) findViewById(R.id.user_points);
        textViewPoints.setText("You have 241 points");

        contactsListView = (ListView) findViewById(R.id.contact_list);

        contactsList = wifiDirectManager.getUsersNearBy();

        mAdapter = new UserContactArrayAdapter(this, contactsList);

        contactsListView.setAdapter((ListAdapter) mAdapter);

        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserContact contact = contactsList.get(position);
                sendPoints(contact);
            }
        });
    }

    public void update(){
        mAdapter.notifyDataSetChanged();
    }

    public ArrayList<UserContact> getUsersNearBy() {
        ArrayList<UserContact> users = new ArrayList<UserContact>();
        users.add(new UserContact("Nuno Dias", R.drawable.profile_image, 136, null));
        users.add(new UserContact("Ricardo Oliveira", R.drawable.profile_image, 2067, null));
        users.add(new UserContact("Pedro Nuno", R.drawable.profile_image, 49, null));
        users.add(new UserContact("Rita Reis", R.drawable.profile_image_female, 3625, null));
        users.add(new UserContact("Alberto Domingues", R.drawable.profile_image, 6524, null));
        users.add(new UserContact("Tiago Rodrigues", R.drawable.profile_image, 10254, null));
        users.add(new UserContact("Ana Paula", R.drawable.profile_image_female, 369, null));
        return users;
    }

    public void sendPoints(UserContact contact){
        Intent intent = new Intent(getApplication(), SendPointsActivity.class);
        intent.putExtra("user-name", contact.getName());
        intent.putExtra("user-image", contact.getImage());
        intent.putExtra("user-points", contact.getPoints());
        intent.putExtra("user-contact-address", contact.getVirtualIp());
        startActivity(intent);
    }
}
