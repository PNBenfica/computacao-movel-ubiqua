package pt.ulisboa.tecnico.cmov.ubibike;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pt.ulisboa.tecnico.cmov.ubibike.domain.Coordinate;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Route;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.ErrorCodes;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RemoteServices;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RenewTokenService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.StoreTrajectoryService;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.UrlResources;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.Beacons;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

/**
 * Created by paulo on 13/04/2016.
 */
public class GPSListener  implements LocationListener {

    private Activity activity;

    // true if user is currently riding
    // if is user is riding, the positions are recorded
    private boolean isRiding;
    private Beacons beacons;

    // list that records the current trajectory coordinates
    private List<Coordinate> trajetoryCoords;

    public GPSListener(Activity activity){
        this.activity = activity;
        WifiDirectManager wifiDirectManager =  ((UbiBikeApplication) this.activity.getApplication()).getWifiDirectManager();
        this.beacons = wifiDirectManager.getBeaconManager();

    }
    public void start(){
        // Setup Location manager and receiver
        LocationManager lManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        try {
            lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
            lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        }
        catch (SecurityException e) {
            Toast.makeText(activity, "Error in gps tracking permissions", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("GPS-Changed", "Location Changed " + location.toString());
        // Toast.makeText(activity, "lat: " + location.getLatitude() + " | lon: " + location.getLongitude(), Toast.LENGTH_SHORT).show();

        if(beacons.departed()){
            startNewTrajectory();
        }

        if (isRiding){
            recordCoordinate(location);
        }

        if(beacons.arrived()){
            beacons.drop(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    // method called when the app detects the beacon of the booked bike
    public void startNewTrajectory(){
        this.trajetoryCoords = new ArrayList<Coordinate>();
        isRiding = true;
    }

    // method called when the app detects that the bike was dropped off
    public void endTrajectory(){
        isRiding = false;
        double distance = calculateDistance(trajetoryCoords);
        int points = calculatePoints(distance);
        String coords = convertToString(trajetoryCoords);
        String date = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date());
        storeInServer(coords, distance, points, date);
        Toast.makeText(activity, "You earned: " + points + " points!", Toast.LENGTH_LONG).show();
    }

    // converts the list of coordinates to a list of string
    private String convertToString(List<Coordinate> coords) {
        String trajetoryCoords = "";
        for(Coordinate coord: coords){
            trajetoryCoords += "&coord=" + coord.toString();
        }
        return trajetoryCoords;
    }

    // records a coordinates in the list
    private void recordCoordinate(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        Coordinate coordinate = new Coordinate(latitude, longitude);
        this.trajetoryCoords.add(coordinate);
    }

    // returns the distance of this trajectory
    private double calculateDistance(List<Coordinate> trajetoryCoords) {
        double distance = 0;
        if(trajetoryCoords.size() > 1){
            for(int i = 1; i < trajetoryCoords.size(); i++){
                Coordinate previousCoord = trajetoryCoords.get(i-1);
                Coordinate currentCoord = trajetoryCoords.get(i);
                distance += calculateDistance(previousCoord, currentCoord);
            }
        }
        return distance;
    }

    private int calculatePoints(double distance){
        return (int) distance * 3;
    }

    private double calculateDistance(Coordinate coord1, Coordinate coord2) {
        Location location1 = new Location("");
        location1.setLatitude(coord1.getLatitude());
        location1.setLongitude(coord1.getLongitude());

        Location location2 = new Location("");
        location2.setLatitude(coord2.getLatitude());
        location2.setLongitude(coord2.getLongitude());

        double distanceInMeters = location1.distanceTo(location2);

        return distanceInMeters * 0.001;
    }

    private void storeInServer(String coords, double distance, int points, String date) {
        StoreTrajectoryService service = new StoreTrajectoryService(activity, coords, distance, points, date);
        service.execute();
    }
}
