package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import pt.ulisboa.tecnico.cmov.ubibike.services.StoreTaskService;

/**
 * Created by paulo on 10/05/2016.
 */
public class StoreTrajectoryService {

    private Activity activity;
    private String params;


    public StoreTrajectoryService(Activity activity, String params){
        this.activity = activity;
        this.params = params;
    }

    public StoreTrajectoryService(Activity activity, String coords, double distance, int points, String date){
        this.activity = activity;
        this.params = coords + "&points="+points+"&date="+date+"&distance="+distance;
    }

    public void execute() {

        final String url = UrlResources.URL() + "trajectories/newtrajectory?"+params;

        RemoteServices.get(activity, url, true, true, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {

                    }
                    // Else display error message
                    else {
                        int error = obj.getInt("error_code");
                        if (error == ErrorCodes.TOKEN_EXPIRED) {
                            renewToken();
                        }
                    }
                } catch (JSONException e) {
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                StoreTaskService.store(activity, url);
            }
        });
    }


    private void renewToken() {
        RenewTokenService renewTokenService = new RenewTokenService(activity);
        renewTokenService.execute();
    }
}
