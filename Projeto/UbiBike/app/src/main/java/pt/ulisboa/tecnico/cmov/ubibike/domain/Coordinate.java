package pt.ulisboa.tecnico.cmov.ubibike.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by paulo on 03/04/2016.
 */
public class Coordinate implements Parcelable {
    private double latitude;
    private double longitude;

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    // The following methods that are required for using Parcelable
    private Coordinate(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(latitude);
        out.writeDouble(longitude);
    }

    // Just cut and paste this for now
    public int describeContents() {
        return 0;
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<Coordinate> CREATOR = new Parcelable.Creator<Coordinate>() {
        public Coordinate createFromParcel(Parcel in) {
            return new Coordinate(in);
        }

        public Coordinate[] newArray(int size) {
            return new Coordinate[size];
        }
    };

    @Override
    public String toString(){
        return "" + getLatitude() + ";" + getLongitude();
    }
}