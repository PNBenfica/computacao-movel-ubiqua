package pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain;

import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;

/**
 * Created by goncaloceia on 28-04-2016.
 */
public class PointsMessage extends Communication{

    private String username;
    private int points;
    private String transactionid;



    public PointsMessage(String message){
        super(message);
        setType(MessageTypes.USERPOINTS);
    }

    public String getTransactionid(){ return this.transactionid;}

    public PointsMessage(String username, int points){
        super(username);
        setType(MessageTypes.USERPOINTS);
        this.username = username;
        this.points = points;
        this.transactionid = SharedPreferencesCodes.generateTransactionId();

    }

    @Override
    public String generateMessage(){
        return getType() + MessageTypes.DELIMITER +
               this.username + MessageTypes.DELIMITER + this.points + MessageTypes.DELIMITER + this.transactionid;
    }

}
