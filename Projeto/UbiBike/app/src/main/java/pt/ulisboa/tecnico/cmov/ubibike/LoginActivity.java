package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


import pt.ulisboa.tecnico.cmov.ubibike.services.remote.LoginService;


public class LoginActivity extends AppCompatActivity {

    Boolean check = false;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        String username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");
        String password = sharedPreferences.getString(SharedPreferencesCodes.PASSWORD, "PasswordNotFound");
        if(username != null && password != null) {
            LoginService loginService = new LoginService(username, password, this);
            loginService.execute();
        }
        */

        setContentView(R.layout.activity_login);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void linkToRegister(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void linkToHome(View view){
        String username;
        String password;
        EditText usernameinput = (EditText)findViewById(R.id.username_input_login_field);
        EditText passwordinput = (EditText)findViewById(R.id.password_input_login_field);
        username = usernameinput.getText().toString();
        password = passwordinput.getText().toString();

        LoginService loginService = new LoginService(username, password, this);
        loginService.execute();
    }
}
