package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.arrayadapters.HomeMenuArrayAdapter;
import pt.ulisboa.tecnico.cmov.ubibike.domain.HomeMenuOption;
import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RetryTasksService;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;

public class HomeActivity extends AppCompatActivity {

    private ArrayAdapter<HomeMenuOption> mAdapter = null;
    private ArrayList<HomeMenuOption> optionsList = null;
    private ListView optionsListView;
    private GPSListener gpsListener;
    private WifiDirectManager wifiDirectManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        profile();

        optionsListView = (ListView) findViewById(R.id.homeMenuList);

        populateMenuList();

        mAdapter = new HomeMenuArrayAdapter(this, optionsList);

        optionsListView.setAdapter((ListAdapter) mAdapter);

        optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HomeMenuOption menuOption = optionsList.get(position);
                // Class<?> option = optionsList.get(position).getOption();
                startActivity(menuOption);
            }
        });

        this.wifiDirectManager = new WifiDirectManager(this);
        ((UbiBikeApplication) this.getApplication()).setWifiDirectManager(wifiDirectManager);

        this.gpsListener = new GPSListener(this);
        this.gpsListener.start();
        ((UbiBikeApplication) this.getApplication()).setGPSListener(gpsListener);


        //   SendPointsService service = new SendPointsService(this, "margarida", 20, "random123");
      //  service.execute();

        // Resends the pending requests
        RetryTasksService retryTasksService = new RetryTasksService(this);
        retryTasksService.execute();
    }

    private void populateMenuList() {
        optionsList = new ArrayList<HomeMenuOption>();
        optionsList.add(new HomeMenuOption("Book Bike", R.drawable.bike, StationsListActivity.class, true));
        optionsList.add(new HomeMenuOption("Messenger", R.drawable.messenger, MessengerContactsActivity.class, true));
        optionsList.add(new HomeMenuOption("Send Points", R.drawable.gift, SendPointsContactsActivity.class, true));
        optionsList.add(new HomeMenuOption("Last Routes", R.drawable.maps, LastRoutesListActivity.class, true));
        optionsList.add(new HomeMenuOption("Logout", R.drawable.logout, LoginActivity.class, false));
    }

    public void startActivity(HomeMenuOption menuOption){
        if (menuOption.requireInternet() && !AppStatus.isNetworkAvailable(this.getApplicationContext())){
            Toast.makeText(getApplicationContext(), "You need a internet connection to perform this action", Toast.LENGTH_LONG).show();
        }
        else{
            Intent intent = new Intent(this, menuOption.getOption());
            startActivity(intent);
        }

    }



    public void profile(){
        TextView textViewusername = (TextView)findViewById(R.id.textView2);
        TextView textViewpoints = (TextView)findViewById(R.id.textView3);
        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
        String username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");
        int points = sharedPreferences.getInt(SharedPreferencesCodes.POINTS, -1);
        textViewusername.setText(username);
        textViewpoints.setText(points + " points");

    }


    public GPSListener getGPSListener(){
        return this.gpsListener;
    }

}
