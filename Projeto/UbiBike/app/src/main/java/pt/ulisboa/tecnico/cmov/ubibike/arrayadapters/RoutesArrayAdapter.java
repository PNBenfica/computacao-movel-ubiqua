package pt.ulisboa.tecnico.cmov.ubibike.arrayadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.R;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Route;

/**
 * Created by paulo on 28/03/2016.
 */
public class RoutesArrayAdapter  extends ArrayAdapter<Route> {

    private final Context context;
    private final ArrayList<Route> routes;

    public RoutesArrayAdapter(Context context, ArrayList<Route> routes) {
        super(context, R.layout.lastroutes_list_item, routes);
        this.context = context;
        this.routes = routes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View routesListView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        routesListView = inflater.inflate(R.layout.lastroutes_list_item, parent, false);

        TextView dateTextView = (TextView) routesListView.findViewById(R.id.route_date);
        dateTextView.setText(routes.get(position).getDate());

        TextView distanceTextView = (TextView) routesListView.findViewById(R.id.route_distance);
        distanceTextView.setText("Distance: " + routes.get(position).getDistance() + "km");

        TextView availableBikesTextView = (TextView) routesListView.findViewById(R.id.points_earned);
        availableBikesTextView.setText("Points earned: " + routes.get(position).getPointsEarned());

        return routesListView;
    }
}
