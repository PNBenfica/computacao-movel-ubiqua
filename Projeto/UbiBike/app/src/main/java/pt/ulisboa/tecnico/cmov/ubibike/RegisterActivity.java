package pt.ulisboa.tecnico.cmov.ubibike;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import pt.ulisboa.tecnico.cmov.ubibike.services.remote.RegisterService;


public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

    }

    public void registUser(View view){
        String username;
        String password;
        String passwordconfirm;
        EditText usernameinput = (EditText)findViewById(R.id.username_input_field);
        EditText passwordinput = (EditText)findViewById(R.id.password_input_field);
        EditText passwordconfirminput = (EditText)findViewById(R.id.passwordconfirm_input_field);
        username = usernameinput.getText().toString();
        password = passwordinput.getText().toString();
        passwordconfirm = passwordconfirminput.getText().toString();
        RegisterService registerService = new RegisterService(username, password, passwordconfirm, this);
        registerService.execute();
    }

}

