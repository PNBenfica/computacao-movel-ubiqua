package pt.ulisboa.tecnico.cmov.ubibike.domain;

/**
 * Created by paulo on 26/03/2016.
 */
public class HomeMenuOption {

    private String name;

    private int image;

    private Class<?> option;

    private boolean requireInternet;

    public HomeMenuOption(String name, int image, Class<?> option, boolean requireInternet){
        this.name = name;
        this.image = image;
        this.option = option;
        this.requireInternet = requireInternet;
    }

    public String getName(){
        return this.name;
    }

    public int getImage(){
        return this.image;
    }

    public Class<?> getOption(){
        return this.option;
    }

    public boolean requireInternet(){ return this.requireInternet; }

}
