package pt.ulisboa.tecnico.cmov.ubibike;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.domain.Coordinate;

public class ViewRouteActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private ArrayList<Coordinate> coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_route);

        setTitleBar();

        setRouteInfo(getIntent());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    private void setTitleBar() {
        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Last Routes");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.maps);
    }

    private void setRouteInfo(Intent intent) {
        String routeDate = intent.getStringExtra("route-date");
        int routeDistance = intent.getIntExtra("route-distance", 0);
        int routePoints = intent.getIntExtra("route-points", 0);
        this.coordinates = intent.getParcelableArrayListExtra("route-coordinates");

        TextView dateTextView = (TextView) findViewById(R.id.route_date);
        dateTextView.setText(routeDate);

        TextView distanceTextView = (TextView) findViewById(R.id.route_distance);
        distanceTextView.setText("Distance: " + routeDistance + "km");

        TextView availableBikesTextView = (TextView) findViewById(R.id.points_earned);
        availableBikesTextView.setText("Points earned: " + routePoints);
    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng initialCoords = new LatLng(coordinates.get(0).getLatitude(), coordinates.get(0).getLongitude());
        mMap.addMarker(new MarkerOptions().position(initialCoords).title("Route started here"));
        LatLng finalCoords = new LatLng(coordinates.get(coordinates.size() - 1).getLatitude(), coordinates.get(coordinates.size() - 1).getLongitude());
        mMap.addMarker(new MarkerOptions().position(finalCoords).title("Route ended here"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(initialCoords, 17));

        PolylineOptions polylineOptions = new PolylineOptions().geodesic(true);
        for(Coordinate coord: coordinates){
            polylineOptions.add(new LatLng(coord.getLatitude(),coord.getLongitude()));
        }

        mMap.addPolyline(polylineOptions);
    }
}
