package pt.ulisboa.tecnico.cmov.ubibike.arrayadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.ubibike.R;
import pt.ulisboa.tecnico.cmov.ubibike.domain.Station;

/**
 * Created by paulo on 27/03/2016.
 */
public class StationsArrayAdapter  extends ArrayAdapter<Station> {

    private final Context context;
    private final ArrayList<Station> stations;

    public StationsArrayAdapter(Context context, ArrayList<Station> stations) {
        super(context, R.layout.station_list_item, stations);
        this.context = context;
        this.stations = stations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View stationsListView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        stationsListView = inflater.inflate(R.layout.station_list_item, parent, false);

        TextView usernameTextView = (TextView) stationsListView.findViewById(R.id.stationName);
        usernameTextView.setText(stations.get(position).getName());

        TextView locationTextView = (TextView) stationsListView.findViewById(R.id.station_location);
        locationTextView.setText("Location: " + stations.get(position).getLocation());

        TextView availableBikesTextView = (TextView) stationsListView.findViewById(R.id.available_bikes);
        availableBikesTextView.setText("Available bikes: " + stations.get(position).getAvailableBikes());

        ImageView stationImageView = (ImageView) stationsListView.findViewById(R.id.station_logo);
        stationImageView.setImageResource(stations.get(position).getImage());

        return stationsListView;
    }
}
