package pt.ulisboa.tecnico.cmov.ubibike.services.remote;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.PrivateKey;

import pt.ulisboa.tecnico.cmov.ubibike.services.GetPrivateKeyService;
import pt.ulisboa.tecnico.cmov.ubibike.services.RSATools;
import pt.ulisboa.tecnico.cmov.ubibike.services.StoreTaskService;

/**
 * Created by paulo on 04/05/2016.
 */
public class SendPointsService {

    private Activity activity;
    private PrivateKey privateKey;
    private String sendToUser;
    private int points;
    private String transactionId;

    public SendPointsService(Activity activity, String sendToUser, int points, String transactionId){
        this.activity = activity;
        this.sendToUser = sendToUser;
        this.points = points;
        this.transactionId = transactionId;

        GetPrivateKeyService getPrivateKeyService = new GetPrivateKeyService(activity);
        getPrivateKeyService.execute();
        this.privateKey = getPrivateKeyService.response();
    }

    private String getSignature(){
        byte[] content = (sendToUser + points + transactionId).getBytes();
        byte[] contentHash = RSATools.generateHash(content);
        byte[] signature = RSATools.signContent(contentHash, this.privateKey);

        Log.d("crypto", "signature: "+URLEncoder.encode(RSATools.encodeToString(signature)));
        return URLEncoder.encode(RSATools.encodeToString(signature));
    }

    public void execute(){
        String params = "username="+sendToUser+"&points="+points+"&transactionid="+transactionId+"&signature="+getSignature();
        final String url = UrlResources.URL() + "sendpoints?" + params;
        RemoteServices.get(activity, url, true, true, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("status")) {
                        // update points
                    }
                } catch (JSONException e) {
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                StoreTaskService.store(activity, url);
            }
        });
    }
}
