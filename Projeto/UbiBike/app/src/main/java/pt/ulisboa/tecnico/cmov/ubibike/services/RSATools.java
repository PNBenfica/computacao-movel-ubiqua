package pt.ulisboa.tecnico.cmov.ubibike.services;

/**
 * Created by paulo on 04/05/2016.
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import android.util.Base64;

public class RSATools {

    public static KeyPair generateKeys() {

        try {

            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

            keyGen.initialize(1024, random);
            KeyPair keys = keyGen.generateKeyPair();
            return keys;

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] generateHash(byte[] array) {
        try {

            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            sha.update(array);
            byte[] hash = sha.digest();
            return hash;

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] signContent(byte[] content, PrivateKey privKey) {

        try {

            Signature dsaForSign = Signature.getInstance("SHA1withRSA");
            dsaForSign.initSign(privKey);
            dsaForSign.update(content);
            byte[] signature = dsaForSign.sign();
            return signature;

        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return content;

    }

    public static boolean validateSignature(byte[] data, byte[] signature, PublicKey publicKey) {
        try {
            byte[] dataHash = generateHash(data);
            Signature dsaForVerify = Signature.getInstance("SHA1withRSA");
            dsaForVerify.initVerify(publicKey);
            dsaForVerify.update(dataHash);
            return dsaForVerify.verify(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
            return false;
        }
    }


    public static String encodeToString(byte[] array) {
        return Base64.encodeToString(array, Base64.DEFAULT);

        /*try {
            return new String(array, "UTF-8");
        }
        catch(Exception e){return null;}*/
    }

    public static PrivateKey decodePrivateKey(String privKey) {
        try {
            return KeyFactory.getInstance("RSA")
                    .generatePrivate(new PKCS8EncodedKeySpec(Base64.decode(privKey, Base64.DEFAULT)));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static byte[] decode(String s) {
        return Base64.decode(s, Base64.DEFAULT);
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        KeyPair keyPair = generateKeys();
        PrivateKey privKey = keyPair.getPrivate();
        PublicKey pubKey = keyPair.getPublic();

        byte[] content = "Hello World".getBytes();
        byte[] contentHash = generateHash(content);
        byte[] signature = signContent(contentHash, privKey);

        boolean isValid = validateSignature(content, signature, pubKey);

        if (isValid) {
            System.out.println("Valid signature");
        } else {
            System.out.println("Invalid signature");
        }
    }
}