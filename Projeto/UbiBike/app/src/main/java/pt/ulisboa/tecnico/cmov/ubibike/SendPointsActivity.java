package pt.ulisboa.tecnico.cmov.ubibike;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.ubibike.services.remote.SendPointsService;
import pt.ulisboa.tecnico.cmov.ubibike.sharedpreferences.SharedPreferencesCodes;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.MessengerOutgoingTask;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.WifiDirectManager;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.MessageRemote;
import pt.ulisboa.tecnico.cmov.ubibike.wifidirect.domain.PointsMessage;

public class SendPointsActivity extends AppCompatActivity {

    private String userName;
    private int userImage;
    private int userPoints;
    private String virtualip;
    private WifiDirectManager wifiDirectManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendpoints);
        wifiDirectManager =  ((UbiBikeApplication) this.getApplication()).getWifiDirectManager();
        wifiDirectManager.setSendPointsActivity(this);

        TextView textViewTitle = (TextView) findViewById(R.id.menu_option_text);
        textViewTitle.setText("Send Points");
        ImageView imageViewTitle = (ImageView) findViewById(R.id.menu_option_image);
        imageViewTitle.setImageResource(R.drawable.gift);

        Intent intent = getIntent();
        userName = intent.getStringExtra("user-name");
        userImage = intent.getIntExtra("user-image", 0);
        userPoints = intent.getIntExtra("user-points", 0);
        virtualip = intent.getStringExtra("user-contact-address");


        TextView usernameTextView = (TextView) findViewById(R.id.contact_name);
        usernameTextView.setText(userName);

        TextView pointsTextView = (TextView) findViewById(R.id.contact_user_points);
        pointsTextView.setText("" + userPoints + " Points");

        ImageView imageView = (ImageView) findViewById(R.id.contact_image);
        imageView.setImageResource(userImage);

    }


    public void sendPoints(View view){

        EditText pointsToSendField = (EditText) findViewById(R.id. points_to_send);
        String pontsToSend = pointsToSendField.getText().toString();
        try {
            int foo = Integer.parseInt(pontsToSend);

            SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPreferencesCodes.CREDENTIALS, Context.MODE_PRIVATE);
            String username = sharedPreferences.getString(SharedPreferencesCodes.USERNAME, "UsernameNotFound");

            PointsMessage pointsMessage = new PointsMessage(username,foo);
            MessengerOutgoingTask messengerOutgoingTask = new MessengerOutgoingTask(wifiDirectManager.getActivity(),this.virtualip);
            String transactionid = pointsMessage.getTransactionid();

            SendPointsService sendPointsService = new SendPointsService(this, userName,foo,transactionid);
            sendPointsService.execute();

            Toast.makeText(this, pointsMessage.generateMessage(), Toast.LENGTH_LONG).show();
            messengerOutgoingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pointsMessage.generateMessage());
        } catch (NumberFormatException e) {
            pointsToSendField.setText("");
            Toast.makeText(this, "Field isn't a number", Toast.LENGTH_LONG).show();
            return;
        }
    }
}